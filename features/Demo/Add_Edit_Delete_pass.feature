Feature: Add Edit Delete Contacts

  Scenario Outline: Add New Contact from Homepage header
    Given User is registered In InTouchApp
    Then User navigates to Homepage
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test1 | User | One | lee | Intouch | Tester1 | QA | Personal | Mobile | 0400123456 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  Scenario Outline: Edit Contact Details
    Given User Launch InTouchApp
    When User searches contact by using "<firstname>"
    Then User Edits Contact with new details  "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"  "<newprefix>" "<newfirstname>" "<newmiddlename>" "<newlastname>" "<newsuffix>"
    Then User Edits company details "<company>" "<newcompany>"
    Then User Edits Phonenumber "<phoneno>" "<newphoneno>"
    And User verifies the Edit Contact using "<newfirstname>"
    And User Share edited contact with ITA user and verify Push Message

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | phoneno | newprefix | newfirstname | newmiddlename | newlastname | newsuffix | newcompany | newphoneno | hiptestuid | hiptest-uid |
      | Mr. | Test1 | User | One | lee | Intouch | 0400123456 | Ms. | Scott | Tiger | two | bee | intouch | 144123456 |  |  |
      
  Scenario Outline: Delete a Contact
    Given User Launch InTouchApp
    When User Searches for contact using "<firstname>"
    Then contact should be displayed Successfully
    Then User should be able to delete from Contact View Page sucessfully
    Then User should be able to delete from contact listing page sucessfully
    And User verifies that deleted Contacts are not present using "<firstname>"

    Examples:
      | firstname | hiptestuid | hiptest-uid |
      | Scott |  |  |
      