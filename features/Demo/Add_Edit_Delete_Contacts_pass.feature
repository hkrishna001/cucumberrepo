Feature: Add Edit Delete Contacts

  Scenario Outline: Add New Contact from HomePage Activity List
    Given User is registered In InTouchApp
    Then User dials a number from T9 Dialer using "<phoneno>"
    Then User selects Greyring Image icon first number on Homepage Activity
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Omc | Usr | Three | lee | Intouch | Tester3 | QA | Personal | Mobile | 0734951279 | Personal | testusertw@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1992 | Facebook | www.fbintouchapp | Personal | 217445 | LancoHills | Hyderabad | 500014 | India | Telangana | Skipphone |  |  |

      
  Scenario Outline: Add New Contact to existing contact in HomePage
    Given User is registered In InTouchApp
    Then User dials a number using T9 dailer "<newphoneno>"
    Then User selects Greyring Image icon for existing user using "<firstname>" on Homepage Activity
    And User verifies the newly added phonenumber for the "<firstname>" "<newphoneno>"

    Examples:
      | firstname | newphoneno | hiptestuid | hiptest-uid |
      | Omc | 1800500235 |  |  |
      
  Scenario Outline: Verify the Added or Edited Contact
    Given User Launch InTouchApp
    When User searches the edited contact using "<firstname>"
    And User Verifies Edited Contact on contact page

    Examples:
      | firstname | hiptestuid | hiptest-uid |
      | Omc |  |  |

   Scenario Outline: Add New Contact to existing contact in T9 Dialer
    Given User is registered In InTouchApp
    Then User dials a number using T9 dailer "<newphoneno>"
    And User verifies newly added phonenumber for the "<firstname>" "<newphoneno>"

    Examples:
      | firstname | newphoneno | hiptestuid | hiptest-uid |
      | Omc | 0022555846 |  |  | 
 