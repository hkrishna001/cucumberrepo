Feature: Add Edit Delete Contacts

  Scenario Outline: Add New Contact from Homepage header
    Given User is registered In InTouchApp
    Then User navigates to Homepage
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test1 | User | One | lee | Intouch | Tester1 | QA | Personal | Mobile | 0400123456 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  