Feature: Search Contacts


  Scenario: T9 Search
    Given User Launch InTouchApp
    Then User able to search contact present in phonebook
    Then User able to call the searched contact
    And Send message to unknown number from T9 Search

 Scenario Outline: Global Search
    Given User Launch InTouchApp 
    Then User able to search Contacts present in phonebook by using "<name>"
    Then User able to search using InTouchApp user by using "<firstname>"
    Then User able to search using "<context>"
    Then User able to search using emoji
    Then User able to search by lives in "<city>"
    Then User able to search by Works at "<company>"
    And User able to search by Works as "<position>"

    Examples:
      | name | firstname | company | position | city | context | hiptest-uid |
      | Test Enhp | Test1 | Microsoft | Manager | Hyderabad | Friend |  |
      
      
      
  Scenario: Verify Contact Listing Page
    Given User Launch InTouchApp
    Then User able to search contact present in contact listing page
    Then User able to sort contacts by first name
    Then User able to sort contacts by last name
    Then User able to sort contacts by time added
    Then User able to sort contacts by time modified
    Then User able to sort contacts by organization
    Then User able to scan business card Successfully
    Then User able to add new list Successfully
    Then User able to edit list Successfully
    Then User able to add contacts to the list
    And User able to remove contacts present in list
        
  Scenario: Save contacts from In my Network On InTouchApp
    Given User Launch InTouchApp
    When contact present in global search
    Then User able to save contacts using FROM MY NETWORK
    And User able to save contacts using ON INTOUCHAPP sucessfully