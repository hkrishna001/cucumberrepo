Feature: Home Page Activities


  Scenario: Home Page Activities
    Given User Launch InTouchApp
    Then User able to see call activity
    Then User able to see connection request activity
    Then User able to see contact share activity
    Then User able to see notice-share activity
    Then User able to send message
    Then User able to set reminder
    And User able to see activity log of the contact present on home page

  Scenario: Do you Remember them functionality
    Given User Launch InTouchApp
    Then User able to open Game using On contact listing page
    Then User able to add emoji context to the contact
    Then User able to share score card on different app
    Then User able to set reminder for this functionality
    Then User able to see contact details of contact
    Then User able to rename the contact name
    And User able to delete the contact

  Scenario: Connect with IntouchApp Users
    Given User Launch InTouchApp
    Then User able to connect with InTouchApp users
    And User Send request to ITA user and Verify

  Scenario Outline: My Cards Functionality
    Given User Launch InTouchApp
    Then User Navigates to mycards section
    Then User able to create new card with provided card data  "cardname" "name" "phonetype" "phonenum" "emailtype" "email" "addresstype" "street1" "street2" "city" "state" "zip" "country" "position" "company" "socialtype" "username" "websitetype" "weburl" "eventtype" "eventname"
    Then User able to edit card details of newly added card
    And User able to share the card sucessfully

    Examples:
      | cardname | name | phonetype | phonenum | emailtype | email | addresstype | street1 | street2 | city | state | zip | country | position | company | socialtype | username | websitetype | weburl | eventtype | eventname | hiptestuid | hiptest-uid |
      | UserCard | Testuser | Personal | 787878 | Personal | Intouch@intouchapp.com | Personal | 21 Hitec | Hitec Park | Hyderabad | Telengana | 500008 | India | Mgr | IntouchApp | AIM | Scott | Home | www.intouchapp.com | Birthday | UserBirthday |  |  |
