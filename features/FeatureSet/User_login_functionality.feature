Feature: User login functionality


  Scenario Outline: Login with New user
    Given User removes previously registered account from device
    Then User Launch InTouchApp
    Then User able to Register to InTouchApp by verifying OTP
    Then User adds profile photo with "username" to the profile
    Then User verifies contact sync after Registration
    Then User able to view all contacts present in phonebook on contact listing page of the InTouchApp
    And User verify cards are displayed after account creation as public personal

    Examples:
      | username | userphoneno | hiptestuid | hiptest-uid |
      | Testnewuser | 9494725325 |  |  |

  Scenario Outline: Login with Existing user
    Given User removes previously registered account from device
    Then User Launches InTouchApp by registering using an existing user "phoneno"
    Then User able to Register to InTouchApp by verifying OTP
    Then verify that "userfirstname" matches for the existing user "phoneno"
    Then User verifies contact sync after Registration
    And User able to view all contacts present in phonebook on contact listing page of the InTouchApp

    Examples:
      | userfirstname | phoneno | hiptestuid | hiptest-uid |
      | Testnewuser | 9494725325 |  |  |
