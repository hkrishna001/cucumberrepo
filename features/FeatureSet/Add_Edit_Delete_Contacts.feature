Feature: Add Edit Delete Contacts


  Scenario Outline: Add New Contact from Homepage header
    Given User is registered In InTouchApp
    Then User navigates to Homepage
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test1 | User | One | lee | Intouch | Tester1 | QA | Personal | Mobile | 0400123456 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  Scenario Outline: Add New Contact from Contact Listing Page header
    Given User is registered In InTouchApp
    Then User navigates to contact listing page
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test2 | User | Two | lee | Intouch | Tester2 | QA | Personal | Mobile | 0400123111 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  Scenario Outline: Add New Contact from HomePage Activity List
    Given User is registered In InTouchApp
    Then User dials a number from T9 Dialer using "<phoneno>"
    Then User selects Greyring Image icon first number on Homepage Activity
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
    And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test3 | User | Three | lee | Intouch | Tester3 | QA | Personal | Mobile | 0123456789 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  Scenario Outline: Add New Contact using T9 Dialer
    Given User is registered In InTouchApp
    Then User adds new contact from T9 Dialer using "<phoneno>"
    Then User Adds Contact with following "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"
    Then User enters following company details "<company>" "<position>" "<department>"
    Then User enters how do you know them emoji context
    Then User enters required details "<phonecategory>" "<phonetype>" "<phoneno>" "<emailtype>" "<emailid>" "<urltype>" "<url>" "<datetype>" "<date>" "<socialurltype>" "<socialurl>" "<addresstype>" "<houseno>" "<locality>" "<city>" "<pincode>" "<country>" "<state>" "<notes>"
   # And User verifies the newly added Contact using "<firstname>"

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | position | department | phonecategory | phonetype | phoneno | emailtype | emailid | urltype | url | datetype | date | socialurltype | socialurl | addresstype | houseno | locality | city | pincode | country | state | notes | hiptestuid | hiptest-uid |
      | Mr. | Test4 | User | Four | lee | Intouch | Tester4 | QA | Personal | Mobile | 0400123333 | Personal | testuser@Intouch.com | Personal | www.intouchapp.com | Biryhday | Aug/01/1990 | Facebook | www.fbintouchapp | Personal | 217485 | LancoHills | Hyderabad | 500014 | India | Telangana | Add new user |  |  |

  Scenario Outline: Add New Contact to existing contact in HomePage
    Given User is registered In InTouchApp
    Then User dials a number using T9 dailer "<newphoneno>"
    Then User selects Greyring Image icon for existing user using "<firstname>" on Homepage Activity
    And User verifies the newly added phonenumber for the "<firstname>" "<newphoneno>"

    Examples:
      | firstname | newphoneno | hiptestuid | hiptest-uid |
      | Auto Enhp | 00012457845 |  |  |
      
      
   Scenario Outline: Add New Contact to existing contact in T9 Dialer
    Given User is registered In InTouchApp
    Then User dials a number using T9 dailer "<newphoneno>"
    And User verifies newly added phonenumber for the "<firstname>" "<newphoneno>"

    Examples:
      | firstname | newphoneno | hiptestuid | hiptest-uid |
      | Auto Enhp | 0002557845 |  |  | 

  Scenario Outline: Edit Contact Details
    Given User Launch InTouchApp
    When User searches contact by using "<firstname>"
    Then User Edits Contact with new details  "<prefix>" "<firstname>" "<middlename>" "<lastname>" "<suffix>"  "<newprefix>" "<newfirstname>" "<newmiddlename>" "<newlastname>" "<newsuffix>"
    Then User Edits company details "<company>" "<newcompany>"
    Then User Edits Phonenumber "<phoneno>" "<newphoneno>"
    And User verifies the Edit Contact using "<newfirstname>"
    And User Share edited contact with ITA user and verify Push Message

    Examples:
      | prefix | firstname | middlename | lastname | suffix | company | phoneno | newprefix | newfirstname | newmiddlename | newlastname | newsuffix | newcompany | newphoneno | hiptestuid | hiptest-uid |
      | Mr. | Test1 | User | One | lee | Intouch | 0400123456 | Ms. | Test | Userone | two | bee | intouch | 144123456 |  |  |

  Scenario Outline: Verify the Added or Edited Contact
    Given User Launch InTouchApp
    When User searches the edited contact using "<firstname>"
    And User Verifies Edited Contact on contact page

    Examples:
      | firstname | hiptestuid | hiptest-uid |
      | Scott |  |  |

  Scenario Outline: Delete a Contact
    Given User Launch InTouchApp
    When User Searches for contact using "<firstname>"
    Then contact should be displayed Successfully
    Then User should be able to delete from Contact View Page sucessfully
    Then User should be able to delete from contact listing page sucessfully
    And User verifies that deleted Contacts are not present using "<firstname>"

    Examples:
      | firstname | hiptestuid | hiptest-uid |
      | Test4 |  |  |
