package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enh.framework.core.Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;  

public class MycardsPage {

   private static AppiumDriver<WebElement> driver;
	
	By lnk_Add = By.id("toolbar_add_button");
	By txt_Addcontact = By.name("Add new contact");
	By txt_cardname = By.id("edittext_input");	
	By btn_ok = By.name("OK");
	By clk_countrycode = By.id("country_code_spinner");	
	By clk_code = By.name("+91 India");
	By txt_num = By.name("Number");
	By txt_email = By.name("Email");
	By clk_Addfield = By.name("Add another field");
	By clk_Address = By.name("Address");
	By txt_street1= By.id("street1");
	By txt_street2 = By.id("street2");
	By txt_city = By.id("city");
	By txt_state = By.id("state");
	By txt_zip = By.id("zip");
	By clk_countryspinner = By.id("country_spinner");
	By clk_India = By.name("India");
	By clk_Organization = By.name("Organization");
	By txt_Position = By.name("Position");
	By txt_Company = By.name("Company");	
	By clk_Social = By.name("Social");
	By txt_SocialUsername = By.name("Username");	
	By clk_Website = By.name("Website");
	By txt_Website = By.name("Website");		
	By clk_Event = By.name("Event");
	By txt_Event = By.name("Website");	
	By clk_date = By.id("event_text");	
	By btn_done = By.name("Done");
    By clk_save = By.name("Save");
    
    By clk_profile = By.id("profile_name");
    By clk_Editprofile = By.id("profile_v2_edit");
    By clk_shareprofile = By.id("profile_v2_share");
    By clk_Chooseanaction = By.name("Choose an action");
    
    
   
	/*"cardname" "name" "phonetype" "phonenum""emailtype" "email" 
	 * "addresstype" "street1" "street2" "city" "state" "zip"  "country" 
	 * "position" "company" "socialtype" "username" "websitetype" "weburl" "eventtype" "eventname"*/
	
	
	static final Logger logger = LoggerFactory.getLogger(MycardsPage.class);
	@SuppressWarnings("unchecked")     
	public MycardsPage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}
	
	
	public void Addcard(String p1, String p2, String p3, String p4, String p5, String p6,
			String p7, String p8, String p9, String p10, String p11, String p12, String p13, 
			String p14, String p15, String p16, String p17, String p18, String p19, String p20, String p21)
	{
		String newcardName = p1 + "_" + Utilities.generateRandomStringwithNumerics();
		System.out.println(newcardName);		
		driver.findElement(lnk_Add).click();
		logger.info("Clicked on Add Contact in My cards is Successfull ");		
		driver.findElement(txt_cardname).sendKeys(newcardName);
		driver.findElement(btn_ok).click();
		logger.info("Profile is dislpayed");  
		driver.findElement(clk_countrycode).click();
		driver.findElement(clk_code).click();
		Utilities.delayFor(1000);
		driver.findElement(txt_num).sendKeys(p4);
	     Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);	
	     Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);	
		driver.findElement(txt_email).sendKeys(p6);
	    driver.findElement(clk_Addfield).click();
		Utilities.delayFor(1000);
	/*	Address section*/
		driver.findElement(clk_Address).click();
		Utilities.delayFor(1000);
		driver.findElement(txt_street1).sendKeys(p8);
		driver.findElement(txt_street2).sendKeys(p9);
		driver.findElement(txt_city).sendKeys(p10);
		driver.findElement(txt_state).sendKeys(p11);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_zip).sendKeys(p12);
		driver.findElement(clk_countryspinner).click();
		Utilities.delayFor(1000);
		driver.findElement(clk_India).click();
		driver.findElement(clk_Addfield).click();
	    /*Organisation section*/
		driver.findElement(clk_Organization).click();
		driver.findElement(txt_Position).sendKeys(p14);
		Utilities.delayFor(1000);
		driver.findElement(txt_Company).sendKeys(p15);
/*		social section*/
		driver.findElement(clk_Addfield).click();
		Utilities.delayFor(1000);
		driver.findElement(clk_Social).click();
		driver.findElement(txt_SocialUsername).sendKeys(p17);
	    /*	Website section*/
		driver.findElement(clk_Addfield).click();
		Utilities.delayFor(1000);
		//driver.findElement(clk_Website).click();
		driver.findElement(txt_Website).sendKeys(p19);
		/*Event section*/
	    driver.findElement(clk_Addfield).click();
		driver.findElement(clk_Event).click();
		Utilities.delayFor(1000);
		driver.findElement(clk_date).click();
		Utilities.delayFor(1000);

		//**************Enter DOB by using Spinner**********//*
		driver.findElementByXPath("//android.widget.EditText[@index='1']").click();
		driver.getKeyboard().sendKeys("Oct");
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		driver.getKeyboard().sendKeys("20");
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		driver.getKeyboard().sendKeys("1990");
		driver.findElement(btn_done).click();
		Utilities.delayFor(1000);
		driver.findElement(clk_save).click();
		Utilities.delayFor(5000);
		//scrollDown();
		Utilities.delayFor(1000);
		Dimension dimens = driver.manage().window().getSize();
		int x = (int) (dimens.getWidth() * 0.5);
		int startY = (int) (dimens.getHeight() * 0.5);
		int endY = (int) (dimens.getHeight() * 0.2);
		for (int i=0;i<30;i++)
		{
		driver.swipe(x, startY, x, endY, 800);
		}
         //verifycard name
		driver.findElementByXPath("//android.widget.TextView[@text='"+newcardName+"']").click();
		Utilities.delayFor(1000);
		driver.findElement(clk_Editprofile).click();
		Utilities.delayFor(1000);
		String newphone = p4 + "1";
		System.out.println(p4);
		System.out.println(newphone);
		driver.findElementByXPath("//android.widget.EditText[@text='"+p4+"']").click();
		driver.findElementByXPath("//android.widget.EditText[@text='"+p4+"']").sendKeys(newphone);
		driver.findElement(clk_save).click();
		Utilities.delayFor(1000);
		driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").click();
		Dimension dimen = driver.manage().window().getSize();
		int x1 = (int) (dimens.getWidth() * 0.5);
		int startY1 = (int) (dimen.getHeight() * 0.5);
		int endY1 = (int) (dimen.getHeight() * 0.2);
		for (int i=0;i<30;i++)
		{
		driver.swipe(x1, startY1, x1, endY1, 800);
		}
		System.out.println(newcardName);
		driver.findElementByXPath("//android.widget.TextView[@text='"+newcardName+"']").click();
		Utilities.delayFor(2000);
		driver.findElement(clk_shareprofile).click();
		Utilities.delayFor(1000);
		if(driver.findElement(clk_Chooseanaction).isDisplayed())
		{
			logger.info("User able to share the card sucessfully");
		}
		
 }	
	
}
