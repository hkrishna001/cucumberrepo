package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.enh.framework.core.Utilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
/**************************************************************************************************
 * Class Name: DailerPage
 * Description : This class contains DailerPage,T9 search contact and call the searched contact														
 * Author : QA-Masters
 * Change History:
 * 
 *             Date                     Modified By                       Change Description
 *             *****                      *********                          ***************
 *                                       QA-Masters                         Initial Draft
 *
 *************************************************************************************************/
public class DailerPage {

	private AppiumDriver<WebElement> driver;

	By Menu_dailer = By.id("menu_dialer");	
	By srch_bar = By.id("search_view");
	By num_enter = By.id("digits");
	
	By btn_phne = By.id("imageView2");
	By btn_phne2 = By.id("action_icon");
	By btn_del = By.id("deleteButton");
	
	By btn_end = By.id("endButton");
	By lnk_add = By.id("btn_add_contact");
	By lnk_addnewcontact = By.name("Add new contact");
	By lnk_Addtoexisting = By.name("Add to existing");	
	By swipe_list = By.id("list");
	By Menu_home = By.id("menu_home");	
	By lbl_headertext = By.id("header_text");	
	
	By clk_message = By.id("message");
	By txt_typemessage = By.name("Type message");
	By clk_sendsms = By.id("send_button_sms");
	
	By lnk_addtocontacts= By.name("Add to contacts");
	By clk_t9btn1 = By.id("btn1");
	
	
	@SuppressWarnings("unchecked")
	public DailerPage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}

	/*********************************************************************************
	 * Function Name: T9 Search
	 * Description : Number Search and call functionality is present in this function
	 **********************************************************************************/
	//Pass Number Search data
/*	public boolean numbersearch()
	{
		if(driver.findElement(Menu_dailer).isDisplayed())
		{
			System.out.println("Contats Menu is displayed");
			driver.findElement(Menu_dailer).click();
			Utilities.delayFor(1000);
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys("0400124");
			if(driver.findElement(btn_phne).isDisplayed())
			{
				System.out.println("Contact is displayed");
				driver.findElement(btn_phne).click();
				Utilities.delayFor(2000);
				if(driver.findElement(btn_end).isDisplayed())
				{
					driver.findElement(btn_end).click();
					System.out.println("Out going call In progress");		
				}
				System.out.println("No Out going cal");
			}
			System.out.println("Contact is not displayed");
		}
		System.out.println("Contats Menu is not displayed");
		return true;
	}*/


	public void DialsANumberFromT9Dialer(String p1)
	{
		if(driver.findElement(Menu_dailer).isDisplayed())
		{
			System.out.println("Dailer Menu is displayed");
			driver.findElement(Menu_dailer).click();
			Utilities.delayFor(2000);
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys(p1);
			if(driver.findElement(btn_phne).isDisplayed())
			{
				driver.findElement(btn_phne).click();
				Utilities.delayFor(5000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
				Utilities.delayFor(2000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);			
				//driver.findElement(swipe_list).click();
				WebElement contact = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));
				int wide  = contact.getSize().width;
				int hgt = contact.getSize().height;
				
				int startx = (int) (wide * (0.1));
				int starty =  hgt/2;	
				int endx = (int)(wide *(0.95));
			    int endy = hgt/2;
			    driver.swipe(startx, starty, endx, endy, 2000);
				if(driver.findElement(Menu_home).isDisplayed())
				{
					driver.findElement(Menu_home).click();
					System.out.println("Menu home is displayed");
				}
			    
			}

		}
		
		
	}
	public boolean DialCustomNumberT9Dialer(String phoneNumber)
	{
		if(driver.findElement(Menu_dailer).isDisplayed())
		{
			System.out.println("Dailer Menu is displayed");
			driver.findElement(Menu_dailer).click();
			Utilities.delayFor(2000);
			driver.findElement(num_enter).click();
			Utilities.delayFor(2000);
			driver.findElement(num_enter).sendKeys(phoneNumber);
			Utilities.delayFor(1000);
			//click  number 1 and delete 
				
			driver.findElement(clk_t9btn1).click();
			Utilities.delayFor(2000);
			driver.findElement(btn_del).click();
			Utilities.delayFor(1000);
			if(driver.findElement(btn_phne).isDisplayed())
			{
				driver.findElement(btn_phne).click();
				Utilities.delayFor(5000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
				Utilities.delayFor(2000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);			
				//driver.findElement(swipe_list).click();
				System.out.println(phoneNumber);
				if(!phoneNumber.contains("0052268"))
				{
					
				WebElement contact = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));
				int wide  = contact.getSize().width;
				int hgt = contact.getSize().height;
				
				int startx = (int) (wide * (0.1));
				int starty =  hgt/2;	
				int endx = (int)(wide *(0.95));
			    int endy = hgt/2;
			    driver.swipe(startx, starty, endx, endy, 2000);

				if(driver.findElement(Menu_home).isDisplayed())
				{
					driver.findElement(Menu_home).click();
					System.out.println("Menu home is displayed");
					Utilities.delayFor(2000);
				}
				}
			}

		}
		return true;
	}
/*	public boolean DialCustomNumberinT9Dialer(String phoneNumber)
	{
		if(driver.findElement(Menu_dailer).isDisplayed())
		{
			System.out.println("Dailer Menu is displayed");
			driver.findElement(Menu_dailer).click();
			Utilities.delayFor(2000);
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys(phoneNumber);
			if(driver.findElement(btn_phne).isDisplayed())
			{
				driver.findElement(btn_phne).click();
				Utilities.delayFor(5000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
				Utilities.delayFor(2000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			}
		}
		return true;
	}*/
	
	public boolean AddsNewContactFromT9Dialer(String p1)
	{
		if(driver.findElement(Menu_dailer).isDisplayed())
		{
			System.out.println("Dailer Menu is displayed");
			driver.findElement(Menu_dailer).click();
			Utilities.delayFor(2000);
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys(p1);
			Utilities.delayFor(2000);			
			driver.findElement(lnk_add).click();
			driver.findElement(lnk_addnewcontact).click();
		}
		return true;
    }
	public boolean AddsExistingFromT9Dialer()
	{
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		//if(driver.findElement(lnk_add).isDisplayed()) 
		if(driver.findElement(lnk_addtocontacts).isDisplayed())
		{		
			System.out.println("Add Contact Icon from Dialer is displayed");
			Utilities.delayFor(2000);
			driver.findElement(lnk_addtocontacts).click();
			Utilities.delayFor(1000);
			driver.findElement(lnk_Addtoexisting).click();		
        }
		//swipeRight();
		return true;
	}
	
	public void T9numberDail()
	{
		if(driver.findElement(btn_phne2).isDisplayed())
		{
			driver.findElement(btn_phne2).click();
			Utilities.delayFor(2000);			
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER); 
			System.out.println("Dailed number and ended the call");	
		}
		System.out.println("Dail icon is not displayed");
	}
	
	public void T9Sendmessage()
	{
		if(driver.findElement(num_enter).isDisplayed())
		{
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys("1800145745");		
			if(driver.findElement(clk_message).isDisplayed())
			{	
				driver.findElement(clk_message).click();
				System.out.println("Clicked on Message icon");			
				driver.findElement(txt_typemessage).sendKeys("Hey John");		
				driver.findElement(clk_sendsms).click();			
			}
			System.out.println("Message icon is not displayed");
		}
		System.out.println("Dailer Menu is not displayed");
		
	}
	
	 public boolean T9numbersearch()
	    {
	    	boolean flag = false;
	    	if(driver.findElement(Menu_dailer).isDisplayed())
			{
				driver.findElement(Menu_dailer).click();
				driver.findElement(num_enter).click();
				driver.findElement(num_enter).sendKeys("0400");
				Utilities.delayFor(1000);
				driver.findElement(clk_t9btn1).click();	
				Utilities.delayFor(1000);
				if(driver.findElement(lnk_addtocontacts).isDisplayed())
				{					
					System.out.println("Contacts is not found in T9 Search");
					flag = false;					
				}
				else
				{
					System.out.println("Contact is found in T9 Search");
					flag = true;
				}
	        }
	    			
	    return flag;

	    }
	 public void swipeRight()
		{
		   	//swipe from dialer
			System.out.println("Swiping to Home from dialer");
	       	WebElement contact = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));
			int wide  = contact.getSize().width;
			int hgt = contact.getSize().height;
			
			int startx = (int) (wide * (0.1));
			int starty =  hgt/2;	
			int endx = (int)(wide *(0.95));
		    int endy = hgt/2;
		    driver.swipe(startx, starty, endx, endy, 2000);
			
		
		}
	
	
}
