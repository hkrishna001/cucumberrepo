package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enh.framework.core.Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class RegistraionPage {
	
private AppiumDriver<WebElement> driver;
	
	By txt_num = By.id("dgts__phoneNumberEditText");
	By clk_getstarted = By.name("Get Started");
	By clk_code = By.id("dgts__countryCode");
	By clk_country = By.name("India +91");
	By clk_send = By.id("dgts__sendCodeButton");	
	By lbl_name = By.id("msg_box1");
	By lbl_num = By.id("msg_box2");
	By clk_Continue = By.name("Continue");
	By clk_agree= By.name("Yes, I agree.");
	By lbl_later = By.name("Do it later");
	By lbl_no = By.name("No, I'm new!");
	By txt_Email = By.id("title");

	By txt_addEmail = By.name("Add new email address");
	By lbl_title = By.id("title");
	By btn_continue= By.id("btn_continue");
	By clk_image1= By.id("image1");
	By lbl_notme = By.name("This is not me!");
	
	By txt_nameofuser= By.id("name_of_user");
	By btn_getin= By.id("btn_get_in");
	
	static final Logger logger = LoggerFactory.getLogger(HomePage.class);
	@SuppressWarnings("unchecked")     
	public RegistraionPage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}
	
	
	public void registration()
	{			
		driver.findElement(clk_getstarted).click();
		driver.findElement(txt_num).sendKeys("9494725325");
		driver.findElement(clk_code).click();
		Dimension dimens = driver.manage().window().getSize();
		int x = (int) (dimens.getWidth() * 0.5);
		int startY = (int) (dimens.getHeight() * 0.2);
		int endY = (int) (dimens.getHeight() * 0.8);
		for (int i=0;i<17;i++)
		{
		driver.swipe(x, startY, x, endY, 800);
		}
		Utilities.delayFor(3000);	
		driver.findElement(clk_country).click();
		driver.findElement(clk_send).click();
		Utilities.delayFor(10000);		
	}
	
	public void Verifyprofile(String p1, String p2)
	{
		String name = driver.findElement(lbl_name).getText();
		String phonenum = driver.findElement(lbl_num).getText();
		Utilities.delayFor(5000);
		System.out.println(p1);
		System.out.println(name);
		System.out.println(p2);
		System.out.println(phonenum);
		if(name.contains(p1) && phonenum.contains(p2))
		{		
			System.out.println("Profile is Matched for existing user");
		}
		else{
			System.out.println("Profile does not Match for existing user");
		}
		driver.findElement(clk_Continue).click();
		driver.findElement(clk_agree).click();
		Utilities.delayFor(3000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);		
		driver.findElement(lbl_later).click();		
		Utilities.delayFor(5000);
		
	}
	
	public void newregistration()
	{		
		String mailid = Utilities.generateRandomStringwithoutNumerics();
		String Mail = mailid + "@gmail.com";
		
		
		String name = Utilities.generateRandomStringwithNumerics();
		String  username = name ;
		
		driver.findElement(lbl_notme).click();	
		driver.findElement(lbl_no).click();			
		driver.findElement(txt_addEmail).sendKeys(Mail);
		driver.findElement(txt_Email).click();	
		System.out.println("Entered Mail Address and waiting");
		Utilities.delayFor(2000);	
		driver.findElement(btn_continue).click();
		driver.findElement(clk_agree).click();
		Utilities.delayFor(5000);
		driver.findElement(txt_nameofuser).sendKeys(username);
		driver.findElement(btn_getin).click();		
		System.out.println("Profile is created ");
		Utilities.delayFor(3000);		
		driver.findElement(clk_image1).click();
		//((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
		Utilities.delayFor(1000);		
		driver.findElement(lbl_later).click();	
	}
	
}
