package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.enh.framework.core.Utilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;



/*****************************************
 * Add New Contact Flow with Page objects*
 *****************************************/




public class Addnewcontact {

	private AppiumDriver<WebElement> driver;

	By btn_Add = By.id("profile_info_contact_image");
	By btn_ExpandName = By.id("expand_name_button");
	By txt_prefix = By.name("Prefix");
	By txt_FirstName = By.name("First name");
	By txt_MiddleName = By.name("Middle name");
	By txt_LastName = By.name("Last name");
	By txt_Suffix = By.name("Suffix");
	By btn_Expandorganisation = By.id("expand_organization");
	By txt_Company = By.name("Company");
	By txt_Position = By.name("Position");
	By txt_Department = By.name("Department");
	By txt_Howdoyouknowthem = By.name("How do you know them?");
	By txt_PhoneNumber = By.name("Phone number");	
	By txt_Email = By.name("Email");
	By btn_Addmorefields = By.name("Add more fields");
	By txt_Url = By.name("URL");
	By txt_Date = By.name("Date");
	By slct_mnth = By.name("Sep");
	By slct_date = By.name("21");
	By slct_yr = By.name("1990");
	By btn_ok = By.id("android:id/button1");	
	By txt_FaceBook = By.name("URL");
	By txt_Houseno = By.name("House no. & building");
	By txt_Locality = By.name("Locality");
	By txt_city = By.name("City");
	By txt_State = By.name("State");
	By txt_Pincode = By.name("Pincode");
	By txt_Country = By.name("Country");
	By txt_India = By.name("India");
	By txt_Addnewnote = By.name("Add new note");
	By txt_Writenotehere = By.name("Write note here");
	By Nav_Back = By.id("toolbar_backbutton");
	By btn_save = By.id("toolbar_save_button");

	By btn_contacts = By.id("menu_contacts");
	By srch_bar = By.id("search_view");
	By lnk_edit = By.name("Edit");
	By btn_tool = By.id("toolbar_add_overflow_container");
	By contacts_info = By.id("contact_info_container");
	By nav_up = By.className("android.widget.ImageButton");
	//========rajesh
	By datepickerYear = By.id("date_picker_header_year");
	By selctMonthArrow = By.id("prev");
	By enertDob = By.id("date_picker_header_date");
	
	

	@SuppressWarnings("unchecked")
	public Addnewcontact(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}
	/*********************************************
	 ********* Enter Details for Add Contact 
	 * @return *****
	 *********************************************/
	public boolean Contactdetails(String p1 , String p2 , String p3 , String p4 , String p5)
	{
		if(driver.findElement(btn_ExpandName).isDisplayed())
		{
			System.out.println("Entering Details");
			driver.findElement(btn_ExpandName).click();	
			driver.findElement(txt_prefix).sendKeys(p1);
			driver.findElement(txt_FirstName).sendKeys(p2);
			driver.findElement(txt_MiddleName).sendKeys(p3);
			driver.findElement(txt_LastName).sendKeys(p4);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			driver.findElement(txt_Suffix).sendKeys(p5);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		}
		else	
		{
			System.out.println("Add contacts formis not dispalyed");
		}
		return true;

	}
	public boolean Companydetails(String company, String position, String department)
	{
		if(driver.findElement(btn_Expandorganisation).isDisplayed())
		{
			driver.findElement(btn_Expandorganisation).click();	
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			driver.findElement(txt_Company).sendKeys(company);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			driver.findElement(txt_Position).sendKeys(position);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			driver.findElement(txt_Department).sendKeys(department);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		}
		else
		{
			System.out.println("Company details are not entered");
		}
		return true;
	}

	public boolean Howyouknowdetails()
	{
		if(driver.findElement(txt_Howdoyouknowthem).isDisplayed())
		{
			driver.findElement(txt_Howdoyouknowthem).sendKeys("Friend");
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			//Add Emoji selection code here

		}
		else
		{
			System.out.println("how you know details are not entered");
		}	
		return true;
	}

	public boolean personaldetails(String phonecategory,String phonetype, String phoneNo,String emailtype, String emailId,String urltype, String uRL,
			String datetype,String date, String socialurltype,String socialurl,String addresstype,
			String houseno, String locality, String city,String pincode,String country,String state,String notes  )
	{
				
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);	
			Utilities.delayFor(2000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			Utilities.delayFor(2000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);	
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			System.out.println(notes);
			if(notes.contains("Skipphone"))
			{
				System.out.println("skipping phone number");
			}
			else{
			System.out.println("Entering phone number");
			driver.findElement(txt_PhoneNumber).click();
			driver.findElement(txt_PhoneNumber).sendKeys(phoneNo);
			}
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);	
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
		driver.findElement(txt_Email).click();
		Utilities.delayFor(1000);
		driver.findElement(txt_Email).sendKeys(emailId);
		Utilities.delayFor(1000);
		//driver.swipe(1000, 1000, 1000, 1000, 3000);
		//swipe(startx, endy, startx, starty, 3000);
		//((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_PAGE_DOWN);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);			
		driver.findElement(btn_Addmorefields).click();	
		System.out.println("clicked on add more fields");
		Utilities.delayFor(1000);
		
		driver.findElement(txt_Url).click();
		driver.findElement(txt_Url).sendKeys(uRL);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		/*((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);*/
		driver.findElement(txt_Date).click();
		
		Utilities.delayFor(10000);

		/**************Enter DOB by using Spinner**********/
		if(driver.findElement(datepickerYear).isDisplayed()){
			/*driver.findElement(datepickerYear).click();
			MobileElement DP = (MobileElement) 

					((AndroidDriver<WebElement>) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()"

					+ ".resourceId(\"android:id/text1\")).scrollIntoView("

					+ "new UiSelector().text(\"2012\"));");
			DP.click();
			driver.findElement(selctMonthArrow).click();
			*/
			driver.findElement(enertDob).sendKeys("Fri, Jan 87");
		}else{
			System.out.println("older calender");
		
		driver.findElementByXPath("//android.widget.EditText[@index='1']").click();
		driver.getKeyboard().sendKeys("Oct");
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		driver.getKeyboard().sendKeys("20");
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		driver.getKeyboard().sendKeys("1990");
		}
		Utilities.delayFor(1000);
		driver.findElement(btn_ok).click();					
		driver.findElement(txt_FaceBook).sendKeys(socialurl); 
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(1000);
		driver.findElement(txt_Houseno).sendKeys(houseno);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_Locality).sendKeys(locality);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_city).sendKeys(city);	
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_Pincode).sendKeys(pincode);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_Country).click();
		driver.findElement(txt_India).click();
		Utilities.delayFor(2000);
		driver.findElement(txt_State).sendKeys(state);
		/*((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_Pincode).click();*/
		//driver.hideKeyboard();
		
		//((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		driver.findElement(txt_Addnewnote).click();
		Utilities.delayFor(1000);
		driver.findElement(txt_Writenotehere).sendKeys(notes);
		Utilities.delayFor(1000);
		driver.findElement(Nav_Back).click();
		driver.findElement(btn_save).click();
		System.out.println("Entered all details");
		//==============rajesh
		try {
			
			
			
			if(driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).isDisplayed()){
				 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();
				 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();

			}
			} catch (Exception e) {
				
			}
		driver.findElement(btn_save).click();
		
		return true;
	}
	//pass
	public boolean verifycontact()
	{
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed");
			driver.findElement(btn_contacts).click();	
			driver.findElement(srch_bar).sendKeys("Test");

			if(driver.findElement(contacts_info).isDisplayed())
			{
				driver.findElement(nav_up).click(); 
				System.out.println("verify new Contact displayed");
			}

		}
		else	
		{
			System.out.println("Add contacts form is not dispalyed");
		}
		return true;
	}
}



