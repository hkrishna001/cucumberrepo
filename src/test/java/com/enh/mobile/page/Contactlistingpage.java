package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.mortbay.log.Log;

import com.enh.framework.core.Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

/**************************************************************************************************
 * Class Name: Contactlistingpage
 * Description : This class contains Contact listing page and contacts sorting
 * 														
 * Author : QA-Masters
 * Change History:
 * 
 *             Date                     Modified By                       Change Description
 *             ****                     **********                          ***************
 *                                       QA-Masters                         Initial Draft
 *
 *************************************************************************************************/

public class Contactlistingpage {
	
	private AppiumDriver<WebElement> driver;

	By btn_contacts = By.id("menu_contacts");
	By srch_bar = By.id("search_view");
	By lnk_edit = By.name("Edit");					
	By contacts_info = By.id("contact_info_container");	
	By Headertext = By.id("header_text");
	By btn_home = By.id("menu_home");	
	By clk_filter = By.id("select_order_button");
	By clk_fn = By.name("First name");	
	By clk_ln = By.name("Last name");		
	By clk_org = By.name("Organization");		
	By clk_timeadded = By.name("Time added");		
	By lnk_timemodified = By.name("Time modified");	
	
	
	
	
	
	
	
	static final Logger logger = LoggerFactory.getLogger(Contactlistingpage.class);
	@SuppressWarnings("unchecked")
	public Contactlistingpage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}

	/*********************************************************
	 * Function Name: Edit contact
	 * Description : Edit contact is present in this function
	 **********************************************************/

	public boolean SearchContactlistingpage(String p1)
	{
		boolean flag = false;
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			Log.info("Contacts link is displayed for Search in Contact listing page ");
			driver.findElement(btn_contacts).click();	
			Utilities.delayFor(5000);
			Log.info("waiting is done");
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(5000);	
			try{
				if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
				{
					Log.info("Contact is not Displayed in search");
					flag = false;
				}
			}
			catch(Exception e)
			{
				Log.info("Contact is Displayed in search");
				flag = true;
			}
		}
		return flag;	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

