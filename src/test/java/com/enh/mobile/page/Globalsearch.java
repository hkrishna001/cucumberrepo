package com.enh.mobile.page;

import org.mortbay.log.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enh.framework.core.Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class Globalsearch {

	private AppiumDriver<WebElement> driver;

	By lbl_headertext = By.id("header_text");
	By srch_bar = By.id("search_view");
	By clk_menu = By.id("menu_search");
	By btn_contacts = By.id("menu_contacts");
	By contacts_info = By.id("contact_info_container");
	By Headertext = By.id("header_text");
	By clk_home = By.id("menu_home");
	By clk_searchclear = By.id("search_clear");
	By clk_emoji = By.id("emoji_selected_text");
	By lbl_emoji = By.id("emoji_text");



	static final Logger logger = LoggerFactory.getLogger(Globalsearch.class);
	@SuppressWarnings("unchecked")
	public Globalsearch(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}

	/*********************************************************
	 * Function Name: Global search 
	 * Description :Global search contact is present in this function
	 **********************************************************/
	/*public boolean T9numbersearch()
    {
    	boolean flag = false;
    	if(driver.findElement(Menu_dailer).isDisplayed())
		{
			driver.findElement(Menu_dailer).click();
			driver.findElement(num_enter).click();
			driver.findElement(num_enter).sendKeys("040012414");
			driver.findElement(clk_t9btn1).click();				
			if(driver.findElement(lnk_addtocontacts).isDisplayed())
			{					
				System.out.println("Contacts is not found in T9 Search");
				flag = false;					
			}
			else
			{
				System.out.println("Contact is found in T9 Search");
				flag = true;
			}
        }

    return flag;

    }*/


	public boolean Globalsearchcontact(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_menu).isDisplayed())
		{
			driver.findElement(clk_menu).click();
			Log.info("Menu Search is displayed");
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(3000);
			try{

				if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
				{
					/*flag = driver.findElement(lbl_headertext).getText().contains(p1);		
			if (flag == true)*/
					Log.info("Contact is not Displayed in Global search");
					flag = false;
				}
			}
			catch(Exception e)
			{
				Log.info("Contact is Displayed in Global search");
				flag = true;
			}
		}
		return flag;	
	}

	public boolean searchInTouchAppuser(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_searchclear).isDisplayed())

			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(srch_bar).sendKeys(p1);
		Utilities.delayFor(3000);
		try{

			if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
			{
				Log.info("InTouch App user is not Displayed in Global search");
				flag = false;
			}
		}
		catch(Exception e)
		{
			Log.info("InTouch App user is Displayed in Global search");
			flag = true;

		}
		return flag;	

	}
	public boolean searchbycontext(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_searchclear).isDisplayed())
			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(srch_bar).sendKeys(p1);
		Utilities.delayFor(3000);
		try{
			if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
			{
				Log.info("context is not Displayed in Global search");
				flag = false;
			}
		}
		catch(Exception e)
		{
			Log.info("context is Displayed in Global search");
			flag = true;
		}
		return flag;		
	}

	public void searchbyemoji()
	{		
		if(driver.findElement(clk_searchclear).isDisplayed())
			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(clk_emoji).click();
		Utilities.delayFor(2000);
		driver.findElementByXPath("//android.widget.ImageButton[@index='1'])").click();
		driver.findElementByXPath("//android.widget.ImageView[@index='0'])").click();
		Utilities.delayFor(2000);
		if(driver.findElement(lbl_emoji).isDisplayed())
		{
			Log.info("Emoji Displayed in Global search");
		}
		else{
			Log.info("Emoji is not Displayed in Global search");
		}	
	}

	public boolean searchbycity(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_searchclear).isDisplayed())
			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(srch_bar).sendKeys(p1);
		Utilities.delayFor(3000);
		try{
			if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
			{
				Log.info("Search by city is not Displayed in Global search");
				flag = false;
			}
		}
		catch(Exception e)
		{
			Log.info("Search by city is Displayed in Global search");
			flag = true;
		}
		return flag;
	}

	public boolean searchbycompany(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_searchclear).isDisplayed())
			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(srch_bar).sendKeys(p1);
		Utilities.delayFor(3000);
		try{
			if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
			{
				Log.info("Search by company is not Displayed in Global search");
				flag = false;
			}
		}
		catch(Exception e)
		{
			Log.info("Search by company is Displayed in Global search");
			flag = true;
		}
		return flag;
	}

	public boolean searchbyposition(String p1)
	{
		boolean flag = false;
		if(driver.findElement(clk_searchclear).isDisplayed())
			driver.findElement(clk_searchclear).click();
		Log.info("Menu Search is displayed");
		driver.findElement(srch_bar).sendKeys(p1);
		Utilities.delayFor(3000);
		try{
			if(driver.findElementByXPath("//android.widget.TextView[@text='No results!' and @index='1']").isDisplayed())
			{
				Log.info("Search by position is not Displayed in Global search");
				flag = false;
			}
		}
		catch(Exception e)
		{
			Log.info("Search by position is Displayed in Global search");
			flag = true;
		}
		return flag;
	}




}



