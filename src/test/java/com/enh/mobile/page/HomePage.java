package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.enh.framework.core.Driverfactory;
import com.enh.framework.core.Utilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.StartsActivity;

public class HomePage {

	private AppiumDriver<WebElement> driver;
	
	By btn_Add = By.id("action_add");
	By txt_Addcontact = By.name("Add new contact");	
	By txt_Addtoexisting = By.name("Add to existing");
	By txt_Activity = By.name("ACTIVITY");
	By btn_Profile = By.id("profile_photo");
	By btn_Home = By.id("menu_home");
	By btn_Save = By.id("Save");
	By txt_share = By.id("info_text");
	By lnk_share = By.id("action_icon");
	By lnk_home = By.id("menu_home");
	By lnk_greyring = By.id("grey_ring");
	By lnk_profile = By.id("action_profile");	
	By lbl_Public = By.name("Public");
	By lbl_Personal = By.name("Personal");	
	By lnk_navback = By.id("toolbar_backbutton");
	By btn_Login = By.name("Log In");
	By nav_up = By.className("android.widget.ImageButton");
	By txt_username = By.id("username");
	By txt_password = By.id("password");
	By btn_logn = By.id("login_btn");
	By btn_continue_login = By.id("continue_with_login");
	By lnk_Home = By.id("menu_home");	
	By txt_context = By.id("contact_context_textview");
	
	static final Logger logger = LoggerFactory.getLogger(HomePage.class);
	@SuppressWarnings("unchecked")     
	public HomePage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}
	public void allowAppPermission(){
		try {
			
			
			
			if(driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).isDisplayed()){
				 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();
				 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();

			}
			} catch (Exception e) {
				System.out.println("Info: permission popup1 not shown");
				
			}
		try{
		
		if(driver.findElement(MobileBy.id("button1")).isDisplayed()){
			driver.findElement(MobileBy.id("button1")).click();

		}
		} catch (Exception e) {
			System.out.println("Info: permission popup2 not shown");
			
		}

		
		}
	public void login() 
	{
		if(driver.findElement(btn_Login).isDisplayed()){
		driver.findElement(btn_Login).click();	
		driver.findElement(txt_username).sendKeys("autoenhp@gmail.com");
		driver.findElement(txt_password).sendKeys("Enhops@123");
		driver.findElement(btn_logn).click();
		
		Utilities.delayFor(2000);
		try {
			
		
		
		if(driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).isDisplayed()){
			 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();
			 driver.findElement(MobileBy.xpath("//*[@class='android.widget.Button'][2]")).click();

		}
		} catch (Exception e) {
			
		}

		driver.findElement(btn_continue_login).click();
		}else{
			System.out.println("application already login");
		}
		/*try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 ((StartsActivity) driver).startActivity("com.android.settings", "com.android.settings.Settings");
		
		 
		 MobileElement radioGroup = (MobileElement) ((AndroidDriver<WebElement>) driver).findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()"

            + ".resourceId(\"com.android.settings:id/title\")).scrollIntoView("

            + "new UiSelector().text(\"Accounts\"));");
            radioGroup.click();
            
            driver.findElement(By.name("InTouchApp")).click();*/
	}
	
	public boolean Addcontact()
	{
		
		
		driver.findElement(lnk_home).click();
		driver.findElement(btn_Add).click();
		driver.findElement(txt_Addcontact).click();
		logger.info("Clicked on Add Contact");
		return false;
		
 }
	/*********************************************************
	* Function Name: UserAbleToSeeCallActivity
	* Description : Call Activity is present in this function
	**********************************************************/
	public boolean userAbleToSeeCallActivity()
	{
		if(driver.findElement(btn_Home).isDisplayed())
		{
			driver.findElement(btn_Home).click();
			Utilities.delayFor(1000);
			logger.info("Clicked on Home");
			if(driver.findElement(txt_Activity).isDisplayed())
			{
				logger.info("Activity Text displayed");
				if(driver.findElement(btn_Profile).isDisplayed())
				{
					logger.info("Call activity displayed is Successfull");
				}
				logger.info("Call activity is not displayed");
			}
			logger.info("Activity Text is not displayed");
		}
		return false;		
	
	}
	
	public boolean userAbleToSeeConnectionRequestActivity()
	{
		if(driver.findElement(btn_Home).isDisplayed())
		{
			driver.findElement(btn_Home).click();
			Utilities.delayFor(1000);
			logger.info("Clicked on home is  Successfull");
	        if(driver.findElement(btn_Save).isDisplayed())
	        {
	        	logger.info("Connection request activity is displayed Successfull");
	        }
		}
		return false;
	}
	
	public boolean userAbleToSeeContactShareActivity()
	{
		if(driver.findElement(btn_Home).isDisplayed())
		{
			driver.findElement(btn_Home).click();
			Utilities.delayFor(1000);
			logger.info("Clicked on home is  Successfull");
	        if(driver.findElement(txt_share).isDisplayed())
	        {
	        	logger.info("contact share activity is displayed Successfull");
	        }
	        
		}
		return false;
	}

	public boolean userAbleToSeeNoticeshareActivity()
	{
		if(driver.findElement(btn_Home).isDisplayed())
		{
			driver.findElement(btn_Home).click();
			Utilities.delayFor(1000);
			logger.info("Clicked on home is  Successfull");
	        if(driver.findElement(lnk_share).isDisplayed())
	        {
	        	logger.info("Notice share activity is displayed Successfull");
	        }
	        
		}
		return false;
	}
	
	public void GreyRingImage()
	{
		if(driver.findElement(lnk_greyring).isDisplayed())
		{
			driver.findElement(lnk_greyring).click();
			driver.findElement(txt_Addcontact).click();
	     }
	}
	
	
	public void GreyRingImageselect()
	{
		
		
		if(driver.findElement(lnk_greyring).isDisplayed())
		{
			driver.findElement(lnk_greyring).click();
			Utilities.delayFor(1000);
			driver.findElement(txt_Addtoexisting).click();
			Utilities.delayFor(1000);
	     }
	}
	
	
	public void verifynewphone(String p1,String p2)
	{
		if(driver.findElement(lnk_greyring).isDisplayed())
		{
			System.out.println("Verifying New Contact number - " + p2);
			driver.findElement(lnk_greyring).click();
			Utilities.delayFor(2000);
			driver.findElement(txt_context).click();
			Utilities.delayFor(2000);
			if(driver.findElementByXPath("//android.widget.TextView[@text='"+p2+"']").isDisplayed())
				{
				Utilities.delayFor(3000);
				System.out.println("New Contact number Verified");
				}
			Utilities.delayFor(3000);
			driver.findElement(nav_up).click();
			Utilities.delayFor(2000);
			//Swipe Right
		/*	WebElement contact = driver.findElement(By.className("android.support.v7.widget.RecyclerView"));
			int wide  = contact.getSize().width;
			int hgt = contact.getSize().height;
			
			int startx = (int) (wide * (0.1));
			int starty =  hgt/2;	
			int endx = (int)(wide *(0.95));
		    int endy = hgt/2;
		    Utilities.delayFor(2000);
		    driver.swipe(startx, starty, endx, endy, 2000);
		    Utilities.delayFor(2000);
			if(driver.findElement(lnk_Home).isDisplayed())
			{
				driver.findElement(lnk_Home).click();
				System.out.println("Menu home is displayed");
			}*/
		}
	}
	
	
	
	public void MycardsProfile()
	{
		if(driver.findElement(lnk_profile).isDisplayed())
		{
			driver.findElement(lnk_profile).click();
		}
	}
	
	public void verifycardsvisibility()
	{
		if(driver.findElement(lnk_profile).isDisplayed())
		{
			driver.findElement(lnk_profile).click();
			assert (driver.findElement(lbl_Public).isDisplayed());
			assert (driver.findElement(lbl_Personal).isDisplayed());
			driver.findElement(lnk_navback).click();
			
			
			
			
			
		}
	}
	
	public void removeUser(){
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
