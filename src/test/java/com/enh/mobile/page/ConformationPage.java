package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.enh.framework.core.Utilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class ConformationPage {

	private AppiumDriver<WebElement> driver;

	By btn_done = By.name("DONE");
    By chk_share = By.id("shareback_contact");
    By btn_skip = By.name("SKIP");
    By btn_addtolist = By.name("Add to list");
    By nav_up = By.className("android.widget.ImageButton");
    

	@SuppressWarnings("unchecked")
	public ConformationPage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}
	/*********************************************
	 ********* Enter Details for Add Contact *****
	 *********************************************/
	public void conformationpage()
	{

		if(driver.findElement(btn_done).isDisplayed())
		{
			System.out.println("Conformation Page is displayed");
			driver.findElement(nav_up).click();
			Utilities.delayFor(2000);
		}
		
	}
	public void Editconformationpage()
	{

		if(driver.findElement(nav_up).isDisplayed())
		{
			System.out.println("Edit Confirmation Page is displayed");
			System.out.println("Navigating to Home Page");
			//if (driver.findElement(nav_up).isDisplayed())
				//driver.findElement(nav_up).click();
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
			Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);

			Utilities.delayFor(2000);
		}
		
	}
	
}