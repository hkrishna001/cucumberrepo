package com.enh.mobile.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.enh.framework.core.Utilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;


/**************************************************************************************************
 * Class Name: ContactsPage
 * Description : This class contains Editcontact and verifications of Editcontact,Delete contact 
 * 															and verification of Delete contact
 * Author : QA-Masters
 * Change History:
 * 
 *             Date                     Modified By                       Change Description
 *             *****                      *********                          ***************
 *                                       QA-Masters                         Initial Draft
 *
 *************************************************************************************************/

public class ContactsPage {

	private AppiumDriver<WebElement> driver;

	By btn_contacts = By.id("menu_contacts");
	By srch_bar = By.id("search_view");
	By lnk_edit = By.name("Edit");

	//By btn_dots = By.className("//android.widget.ImageView");
	//By btn_dots = By.xpath("//android.widget.ImageView[@content-desc='More options']");
	By btn_dots = By.xpath("//android.widget.TextView[@index='2']");
	By edit_click = By .id("contact_context_textview");
	By contacts_info = By.id("contact_info_container");
	By contacts_info1 = By.id("first_line");
	By lnk_delete = By.name("Delete");
	By lnk_Yes = By.name("Yes");
	By txt_noresult = By.name("No results!");
	By lbl_contactinfo = By.id("contact_info_container");
	
	By btn_ExpandName = By.id("expand_name_button");
	By txt_prefix = By.name("Prefix");
	By txt_FirstName = By.name("First name");
	By txt_MiddleName = By.name("Middle name");
	By txt_LastName = By.name("Last name");
	By txt_Suffix = By.name("Suffix");
	/*By txt_prefix = By.id("edittext");
	By txt_FirstName = By.id("edittext");
	By txt_MiddleName = By.id("edittext");
	By txt_LastName = By.id("edittext");
	By txt_Suffix = By.id("edittext");*/
	By btn_save = By.id("toolbar_save_button");
	By nav_up = By.className("android.widget.ImageButton");
	By Headertext = By.id("header_text");
	By Add_contactlist = By.id("action_add_contactlist");
	By lnk_AddNewcontact = By.name("Add new contact");
	By btn_home= By.id("menu_home");
	By btn_Expandorganisation = By.id("expand_organization");
	By txt_Company = By.name("Company");
	By txt_Position = By.name("Position");
	By txt_Department = By.name("Department");
	By txt_Howdoyouknowthem = By.name("How do you know them?");
	By txt_PhoneNumber = By.name("Phone number");	
	By txt_Email = By.name("Email");
	By btn_Addmorefields = By.name("Add more fields");
	By txt_Url = By.name("URL");
	By txt_Date = By.name("Date");
	By slct_mnth = By.name("Sep");
	By slct_date = By.name("21");
	By slct_yr = By.name("1990");
	By btn_ok = By.id("android:id/button1");	
	By txt_FaceBook = By.name("URL");
	By txt_Houseno = By.name("House no. & building");
	By txt_Locality = By.name("Locality");
	By txt_city = By.name("City");
	By txt_State = By.name("State");
	By txt_Pincode = By.name("Pincode");
	By txt_Country = By.name("Country");
	By txt_India = By.name("India");
	By txt_Addnewnote = By.name("Add new note");
	By txt_Writenotehere = By.name("Write note here");
	By Nav_Back = By.id("toolbar_backbutton");
	By txt_search = By.id("search_src_text");
	By clk_image = By.id("contactbook_contact_photo");	
	By clk_save = By.id("toolbar_save_button");	
	By clk_contactdots = By.id("toolbar_add_overflow_container");
	By clk_syncnow = By.name("Sync now");
	By txt_nocontacts = By.name("No contacts found");
    

	@SuppressWarnings("unchecked")
	public ContactsPage(WebDriver driver)
	{
		this.driver =(AndroidDriver<WebElement>) driver;
	}

	/*********************************************************
	 * Function Name: Edit contact
	 * Description : Edit contact is present in this function
	 **********************************************************/

	public boolean searchcontact(String p1)
	{
		boolean flag = false;
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed");
			driver.findElement(btn_contacts).click();	
			Utilities.delayFor(5000);
			System.out.println("waiting is done");
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(5000);
			if(driver.findElement(contacts_info).isDisplayed())
			{
				flag = driver.findElement(Headertext).getText().contains(p1);		
				if (flag == true)
					System.out.println("Contact is found in search");
				//driver.findElement(nav_up).click();
				driver.findElement(btn_home).click();	
				System.out.println("Navigated back to Home Screen");
			}		
		}
		else	
		{
			System.out.println("Contacts bar is not displayed");
		}
		return flag;
	}

	public boolean EditedContactSearch(String p1)
	{
		boolean flag = false;
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed");
			driver.findElement(btn_contacts).click();	
			Utilities.delayFor(5000);
			//System.out.println("waiting is done");
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(5000);
			if(driver.findElement(contacts_info).isDisplayed())
			{
				System.out.println("Edited contact view is displayed");
				String s1=driver.findElement(Headertext).getText();
				System.out.println("Name Contact HeaderText - " + s1);
				System.out.println("Name Contact to be found - " + p1);
				flag = s1.contains(p1);		
				if (flag == true)
					System.out.println("Edited Contact is found in search");
				else
					System.out.println("Edited Contact is not found in search");
			}		
		}
		else	
		{
			System.out.println("Contacts button is not displayed");
		}
		return flag;
	}
	public void verifyeditcontact()
	{
		if(driver.findElement(contacts_info).isDisplayed())
		{
			System.out.println("Verified Edited Contact");
			Utilities.delayFor(1000);
			driver.findElement(btn_home).click();	
		}			
	}

	public boolean navgatecontactlistingpage()
	{
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is for navigation");
			driver.findElement(btn_contacts).click();	
			Utilities.delayFor(5000);
			driver.findElement(Add_contactlist).click();
			Utilities.delayFor(1000);
			driver.findElement(lnk_AddNewcontact).click();
		}
		else
		{
			System.out.println("Navigation is not worked");
		}

		return true;
	}

	public boolean Editcontact(String p1)
	{
		System.out.println("Contact editing started");
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is for edit func");
			driver.findElement(btn_contacts).click();	
			driver.findElement(srch_bar).sendKeys(p1);
			driver.findElement(contacts_info).click();	
			Utilities.delayFor(3000);


			//System.out.println("Waiting for dots to click");
			/*	driver.findElement(btn_dots).click(); */	
			Utilities.delayFor(2000);
			if (driver.findElement(edit_click).isDisplayed())
			{
				//driver.findElement(edit_click).click();	
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			}
			Utilities.delayFor(2000);			
			driver.findElement(lnk_edit).click(); 

		}
		else	
		{
			System.out.println("Contacts bar is not displayed");
		}
		return true;
	}

	public boolean EditContactdetails(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10)
	{
		if(driver.findElement(btn_ExpandName).isDisplayed())
		{

			System.out.println("Entering Edit Details");
			driver.findElement(btn_ExpandName).click();	


			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);		
			driver.findElementByXPath("//android.widget.EditText[@text='"+p1+"']").clear();
			Utilities.delayFor(2000);
			driver.findElement(txt_prefix).sendKeys(p6);

			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);		
			driver.findElementByXPath("//android.widget.EditText[@text='"+p2+"']").clear();
			Utilities.delayFor(2000);
			driver.findElement(txt_FirstName).sendKeys(p7);			

			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			driver.findElementByXPath("//android.widget.EditText[@text='"+p3+"']").clear();
			Utilities.delayFor(2000);
			driver.findElement(txt_MiddleName).sendKeys(p8);		

			/*((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		    driver.findElementByXPath("//android.widget.EditText[@text='"+p4+"']").clear();
		    Utilities.delayFor(3000);
		    driver.findElement(txt_LastName).sendKeys(p9);	*/		

			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);			
			//driver.findElementByXPath("//android.widget.EditText[@text='"+p5+"']").clear();
			driver.findElement(txt_Suffix).click();
			Utilities.delayFor(3000);
			driver.findElement(txt_Suffix).sendKeys(p10);

			//	driver.findElement(btn_save).click();
			System.out.println("Entered and saved Edited contact details ");
		}
		else	
		{
			System.out.println("Add contacts form is not dispalyed");
		}
		return true;
	}
	public boolean EditCompanydetails(String p1, String p2)
	{
		if(driver.findElement(btn_Expandorganisation).isDisplayed())
		{
			Utilities.delayFor(2000);
			System.out.println("Editing company details ");
			driver.findElement(btn_Expandorganisation).click();	
			driver.findElementByXPath("//android.widget.EditText[@text='"+p1+"']").clear();
			driver.findElement(txt_Company).sendKeys(p2);
		}
		else
		{
			System.out.println("Company details are not entered");
		}
		return true;
	}

	public boolean EditHowyouknowdetails()
	{
		if(driver.findElement(txt_Howdoyouknowthem).isDisplayed())
		{
			driver.findElement(txt_Howdoyouknowthem).clear();
			driver.findElement(txt_Howdoyouknowthem).sendKeys("Friend");
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
			//Add Emoji selection code here

		}
		else
		{
			System.out.println("How you know details - Not edited");
		}	
		return true;
	}

	public boolean Editpersonaldetails(String p1, String p2 )
	{
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);	
		Utilities.delayFor(2000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);
		Utilities.delayFor(2000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);	
		Utilities.delayFor(1000);
		((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_TAB);

		/*		driver.findElement(txt_PhoneNumber).click();	
		driver.findElement(txt_PhoneNumber).clear();	
		driver.findElement(txt_PhoneNumber).sendKeys(p2);	*/

		//driver.findElementByXPath("//android.widget.EditText[@text='"+p1+"']").click();

		driver.findElementByXPath("//android.widget.EditText[@text='"+p1+"']").clear();
		driver.findElement(txt_PhoneNumber).sendKeys(p2);

		driver.findElement(btn_save).click();
		System.out.println("Updated Personal Phone number");
		return true;
	}
	/*************************************************************
	 * Function Name: VerifyEdit
	 * Description : Verify Edit contact is present in this function
	 **************************************************************/
	//Pass Data for verify edit
	public boolean VerifyEdit(String p1)
	{
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed");
			driver.findElement(btn_contacts).click();
			Utilities.delayFor(1000);
			driver.findElement(srch_bar).sendKeys(p1);
			if(driver.findElement(contacts_info).isDisplayed())
			{
				driver.findElement(contacts_info).click();
				assert(driver.findElement(contacts_info1).isDisplayed());
				System.out.println("Edited contact info is displayed");
				Utilities.delayFor(3000);
				//driver.findElement(nav_up).click();
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				Utilities.delayFor(1000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				Utilities.delayFor(1000);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
				Utilities.delayFor(2000);
				driver.findElement(btn_home).click();	
				System.out.println("Navigated back to Home Screen");
			}
			else
			{
				System.out.println("Edited contact not found");
			}
		}
		else	
		{
			System.out.println("Contacts bar is not displayed");
		}
		return true;
	}
	/*********************************************************
	 * Function Name: Deletecontact
	 * Description : Delete contact is present in this function
	 **********************************************************/
	public void searchcontactDelete(String p1)
	{
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed");
			driver.findElement(btn_contacts).click();	
			Utilities.delayFor(2000);
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(2000);
			if(driver.findElement(contacts_info).isDisplayed())
			{				
				System.out.println("Contact is found in search");			
			}		
		}
		else	
		{
			System.out.println("Contact is not displayed");
		}
	}


	public void Deletecontactdisplay()
	{	
		if(driver.findElement(contacts_info).isDisplayed())
		{
			System.out.println("Contact to be deleted is found");
		}		
		else	
		{
			System.out.println("Contact to be deleted is not found");
		}	
	}

	public void contactlistingelete()
	{
		if(driver.findElement(contacts_info).isDisplayed())
		{
			driver.findElement(contacts_info).click();
			Utilities.delayFor(1000);
			
			System.out.println("Waiting for DELETE POPUP");
			/*	driver.findElement(btn_dots).click(); */	
			Utilities.delayFor(2000);
			if (driver.findElement(edit_click).isDisplayed())
			{
				//driver.findElement(edit_click).click();	
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_RIGHT);
				((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			}
			Utilities.delayFor(2000);	
			
			//driver.findElement(btn_dots).click();	
			driver.findElement(lnk_delete).click();
			driver.findElement(lnk_Yes).click();
			Utilities.delayFor(2000);
			//driver.findElement(edit_click).click();	
			//Utilities.delayFor(1000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_UP);
			//((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_DPAD_LEFT);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ENTER);
			Utilities.delayFor(1000);
			driver.findElement(btn_home).click();	
			System.out.println("Navigated back to Home Screen");
		}
		else	
		{
			System.out.println("Contacts info is not displayed, Cannot Delete");
		}
	}


	/*****************************************************************
	 * Function Name: VerifyDeletecontact
	 * Description : Verify Delete contact is present in this function
	 ******************************************************************/
	//Pass deleted data 
	public boolean VerifyDeletedcontact(String p1)
	{
		boolean flag = false;
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contcats link is displayed");
			driver.findElement(btn_contacts).click();	
			driver.findElement(srch_bar).sendKeys(p1);
			Utilities.delayFor(2000);
			if(driver.findElement(txt_noresult).isDisplayed())
			{
				System.out.println("Verified - Contact is deleted successfully"); 
				
				flag = true;
			}
			else	
			{
				System.out.println("Contact is not deleted");
				flag = false;
			}
		}
		else	
		{
			System.out.println("Contacts bar is not displayed");
		}
		Utilities.delayFor(1000);
		driver.findElement(btn_home).click();	
		System.out.println("Navigated back to Home Screen after Delete verification");
		return flag;
	}

	public boolean chooseAcontact(String p1)
	{
		boolean flag = false;
		System.out.println("Contacts search");

		driver.findElement(txt_search).click();	
		Utilities.delayFor(3000);
		System.out.println("Entering contact to Search");
		driver.findElement(txt_search).sendKeys(p1);
		Utilities.delayFor(3000);
		try
		{
		
		   	driver.findElement(clk_image).click();
			Utilities.delayFor(3000);
			driver.findElement(clk_save).click();
			Utilities.delayFor(2000);
			flag = true;
		   	
		}
		catch(Exception e)
		{
			System.out.println("Search contact not found");
			flag=false;
		}

		return flag;
	}

	public void contacsync()
	{
		if(driver.findElement(btn_contacts).isDisplayed())
		{
			System.out.println("Contacts link is displayed for SYNC");
			driver.findElement(btn_contacts).click();	

			driver.findElement(clk_contactdots).click();	
			driver.findElement(clk_syncnow).click();			
			Utilities.delayFor(10000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ESCAPE);
			/*Utilities.delayFor(3000);
			((AndroidDriver<WebElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_ESCAPE);*/
		}

	}

	public void navback()
	{
		//nav back to home page from search existing contact page
		driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Navigate up']").click();	 
		driver.findElement(Nav_Back).click();	 
		driver.findElement(lnk_Yes).click();	
	}

    public boolean viewcontacts()
    {
    	boolean flag = false;
    	if(driver.findElement(btn_contacts).isDisplayed())
		{
    		Utilities.delayFor(5000);
			System.out.println("Contcats link is displayed for view contact");
			driver.findElement(btn_contacts).click();
			if(driver.findElement(lbl_contactinfo).isDisplayed())
			{
				
				System.out.println("Contacts are  displayed in contacts listing page");
				flag = true;
				
			}
			else
			{
				System.out.println("No Contacts are  displayed in contacts listing page");
				flag = false;
			}
        }
    	driver.findElement(btn_home).click(); 		
    return flag;

    }
}