package com.enh.testscripts.IntouchApp;

import com.enh.framework.core.Driverfactory;
import com.enh.framework.core.Utilities;
import com.enh.framework.testreports.ReportManager;
import com.enh.mobile.page.Addnewcontact;
import com.enh.mobile.page.ConformationPage;
import com.enh.mobile.page.ContactsPage;
import com.enh.mobile.page.DailerPage;
import com.enh.mobile.page.Globalsearch;
import com.enh.mobile.page.HomePage;
import com.enh.mobile.page.MycardsPage;
import com.enh.mobile.page.RegistraionPage;

public class Actionwords {

    public void userIsRegisteredInInTouchApp() {
    	System.out.println("mobile Execution started");
		Driverfactory.SwitchDriver(1); 		
		Utilities.delayFor(3000);
        System.out.println("Apk is launched"); 
        HomePage homepage = new HomePage(Driverfactory.getSeleniumWebDriver());
    	homepage.allowAppPermission();
       /* 
       HomePage hp = new HomePage(Driverfactory.getSeleniumWebDriver());
        hp.login();*/
    }

    public void userNavigatesToHomepage() {
    	HomePage homepage = new HomePage(Driverfactory.getSeleniumWebDriver());
        homepage.Addcontact();
    }

    public void userAddsContactWithFollowingP1P2P3P4P5(String p1, String p2, String p3, String p4, String p5) {
    	Addnewcontact Ac = new Addnewcontact(Driverfactory.getSeleniumWebDriver());
		boolean f = Ac.Contactdetails(p1, p2, p3, p4, p5);
		if (f)
		{
			ReportManager.LogSuccess("New Contact Creation",
					"Enter details for creation of New Contact",
					" New Contact should be created", 
					"New Contact created", false);		
		} else {
			ReportManager.LogFailure("New Contact Creation",
					"Enter details for creation of New Contact",
					"New Contact should be created", 
							 "New Contact is not created", true);		
		}
    }

    public void userEntersFollowingCompanyDetailsP1P2P3(String p1, String p2, String p3) {
    	Addnewcontact Ac = new Addnewcontact(Driverfactory.getSeleniumWebDriver());
		boolean f = Ac.Companydetails(p1, p2, p3);
		System.out.println("flagvalue = " + f);
		if (f)
		{
			ReportManager.LogSuccess("New Contact Creation",
					"Enter Company details for creation of New Contact",
					" New Contact should be created", "New Contact ",false);		
		} else {
			ReportManager.LogFailure("New Contact Creation",
					"Enter Company details for creation of New Contact",
					"New Contact should be created", "New Contact ",true);		
		}
    }

    public void userEntersHowDoYouKnowThemEmojiContext() {
    	Addnewcontact Ac = new Addnewcontact(Driverfactory.getSeleniumWebDriver());
		boolean f = Ac.Howyouknowdetails();
		System.out.println("flagvalue = " + f);
		if (f)
		{
			ReportManager.LogSuccess("New Contact Creation",
					"Enter How You know details for creation of New Contact",
					" New Contact should be created", "New Contact ",false);		
		} else {
			ReportManager.LogFailure("New Contact Creation",
					"Enter How You know details for creation of New Contact",
					"New Contact should be created", "New Contact ",true);		
		}
    }

    public void userEntersRequiredDetailsP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13, String p14, String p15, String p16, String p17, String p18, String p19) {
    	Addnewcontact Ac = new Addnewcontact(Driverfactory.getSeleniumWebDriver());
		boolean f = Ac.personaldetails(p1, p2, p3, p4, p5, p6, p7, p8, p9,p10, p11, p12,p13,p14,p15, p16, p17, p18, p19);
		
		
		ConformationPage verify = new ConformationPage(Driverfactory.getSeleniumWebDriver());
		verify.conformationpage();
		System.out.println("flagvalue = " + f);
		if (f)
		{
			ReportManager.LogSuccess("New Contact Creation",
					"Enter Personal details for creation of New Contact",
					" New Contact should be created", "New Contact ",false);		
		} else {
			ReportManager.LogFailure("New Contact Creation",
					"Enter Personal details for creation of New Contact",
					"New Contact should be created", "New Contact ",true);		
		}
    }

    public void userVerifiesTheNewlyAddedContactUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.searchcontact(p1);
    }

    public void userNavigatesToContactListingPage() {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());	
    	cp.navgatecontactlistingpage();
    }

    public void userDialsANumberFromT9DialerUsingP1(String p1) {
    	DailerPage dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.DialsANumberFromT9Dialer(p1);
    }

    public void userSelectsGreyringImageIconFirstNumberOnHomepageActivity() {
    	HomePage hp = new HomePage(Driverfactory.getSeleniumWebDriver());
    	hp.GreyRingImage(); 
    }

    public void userAddsNewContactFromT9DialerUsingP1(String p1) {
    	DailerPage dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.AddsNewContactFromT9Dialer(p1);
    }

    public void userDialsANumberUsingT9DailerP1(String p1) {
    	DailerPage dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.DialCustomNumberT9Dialer(p1);
    }
    
    public void userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(String p1) {
    	/*ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	Utilities.delayFor(30000);
    	cp.contacsync();*/
    	HomePage HP = new HomePage(Driverfactory.getSeleniumWebDriver());
    	HP.GreyRingImageselect();
    }

    public void userAddsNewPhoneContactWithP1(String p1) {
    	
    }
    public void userVerifiesNewlyAddedPhonenumberForTheP1P2(String p1,String p2) {
    	DailerPage dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.AddsExistingFromT9Dialer();
    	//
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	boolean flag = cp.chooseAcontact(p1);
    	System.out.println("Choose contact flag");
    	System.out.println(flag);
    	dp.swipeRight();
    	Utilities.delayFor(3000);
    	if(flag == false)
    	{
    		System.out.println("No contact is found in contact list");
    	}  	
    	else{
    		assert(flag == true);
    		
    		HomePage HP = new HomePage(Driverfactory.getSeleniumWebDriver());
    		HP.verifynewphone(p1, p2);
    		System.out.println("New phone verification over");
    	} 	
    	Utilities.delayFor(3000);
    	DailerPage dp1 = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp1.swipeRight();
    }
    
    public void userVerifiesTheNewlyAddedPhonenumberForTheP1P2(String p1,String p2) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	boolean flag = cp.chooseAcontact(p1);
    	
    	if(flag == false)
    	{
    		System.out.println("No contact is found in contact list");
    	}  	
    	else{
    		assert(flag == true);
    		HomePage HP = new HomePage(Driverfactory.getSeleniumWebDriver());
    		HP.verifynewphone(p1, p2);
    	} 	
    	cp.navback(); 
    }
    public void userLaunchInTouchApp() {
    	System.out.println("mobile Execution started");
		Driverfactory.SwitchDriver(1); 		
		Utilities.delayFor(10000);
        System.out.println("Apk is launched"); 
        
    }

    public void userSearchesContactByUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.searchcontact(p1);  	
    	cp.Editcontact(p1);
    }

    public void userVerifiesTheEditContactUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.VerifyEdit(p1);

    }

    public void userShareEditedContactWithITAUserAndVerifyPushMessage() {
    	/* Xpath Issue */ 
    }
    
    
    public void userEditsContactWithNewDetailsP1P2P3P4P5P6P7P8P9P10(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.EditContactdetails(p1, p2, p3, p4, p5 ,p6,p7,p8,p9,p10);
    }

    public void userEditsCompanyDetailsP1P2(String p1, String p2) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.EditCompanydetails(p1,p2);
    }

    public void userEditsPhonenumberP1P2(String p1, String p2) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.Editpersonaldetails(p1,p2);
    	ConformationPage verify = new ConformationPage(Driverfactory.getSeleniumWebDriver());
		verify.Editconformationpage();
    }

    public void userSearchesTheEditedContactUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.EditedContactSearch(p1);
    }

    public void userVerifiesEditedContactOnContactPage() {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.verifyeditcontact();	
    }

    public void userSearchesForContactUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.searchcontactDelete(p1);
    }

    public void contactShouldBeDisplayedSuccessfully() {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.Deletecontactdisplay(); 
    }

    public void userShouldBeAbleToDeleteFromContactViewPageSucessfully() {
    	/*Press and hold need to perform here*/
    }

    public void userShouldBeAbleToDeleteFromContactListingPageSucessfully() {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	cp.contactlistingelete();
    }

    public void userVerifiesThatDeletedContactsAreNotPresentUsingP1(String p1) {
    	ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
    	assert(cp.VerifyDeletedcontact(p1)); 
    }

    public void userAbleToSeeCallActivity() {

    	
    	
    	
    	
    }

    public void userAbleToSeeConnectionRequestActivity() {

    }

    public void userAbleToSeeContactShareActivity() {

    }

    public void userAbleToSeeNoticeshareActivity() {

    }

    public void userAbleToSendMessage() {

    }

    public void userAbleToSetReminder() {

    }

    public void userAbleToSeeActivityLogOfTheContactPresentOnHomePage() {

    }

    public void userAbleToOpenGameUsingOnContactListingPage() {

    }

    public void userAbleToAddEmojiContextToTheContact() {

    }

    public void userAbleToShareScoreCardOnDifferentApp() {

    }

    public void userAbleToSetReminderForThisFunctionality() {

    }

    public void userAbleToSeeContactDetailsOfContact() {

    }

    public void userAbleToRenameTheContactName() {

    }

    public void userAbleToDeleteTheContact() {

    }

    public void userAbleToConnectWithInTouchAppUsers() {

    }

    public void userSendRequestToITAUserAndVerify() {

    }
    public void userNavigatesToMycardsSection() {
    	HomePage hp = new HomePage(Driverfactory.getSeleniumWebDriver());
    	hp.MycardsProfile(); 
    }
    public void userAbleToCreateNewCardWithProvidedCardDataP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19P20P21(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13, String p14, String p15, String p16, String p17, String p18, String p19, String p20, String p21) {
    	MycardsPage mc = new MycardsPage(Driverfactory.getSeleniumWebDriver());
    	mc.Addcard(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21);  	
    }

    public void userAbleToEditCardDetailsOfNewlyAddedCard() {
    	 /*******action are performed in Addcard*****/
    }

    public void userAbleToShareTheCardSucessfully() {
    	 /*******action are performed in Addcard*****/
    }

    public void userAbleToSearchContactPresentInPhonebook() {
    	DailerPage  dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	assert(dp.T9numbersearch());
    }

    public void userAbleToCallTheSearchedContact() {
    	DailerPage  dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.T9numberDail();
    }

    public void sendMessageToUnknownNumberFromT9Search() {
    	DailerPage  dp = new DailerPage(Driverfactory.getSeleniumWebDriver());
    	dp.T9Sendmessage();
    }

   /* public void userAbleToSearchContactsPresentInPhonebookp1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.Globalsearchcontact(p1);    	
    }

    public void userAbleToSearchUsingInTouchAppUser(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchInTouchAppuser(p1);
    }

    public void userAbleToSearchUsingContext(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbycontext(p1);
    }

    public void userAbleToSearchUsingEmoji() {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbyemoji();
    }

    public void userAbleToSearchByLivesInCityName(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbycity(p1);
    }

    public void userAbleToSearchByWorksAtCompanyName(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbycompany(p1);
    }

    public void userAbleToSearchByWorksAsPosition(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbyposition(p1);
    }*/

    public void userAbleToSearchContactsPresentInPhonebookByUsingP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.Globalsearchcontact(p1));
    	//assert(dp.T9numbersearch());
    }

    public void userAbleToSearchUsingInTouchAppUserByUsingP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.searchInTouchAppuser(p1));
    }

    public void userAbleToSearchUsingP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.searchbycontext(p1));
    }

    public void userAbleToSearchUsingEmoji() {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	gs.searchbyemoji();
    }

    public void userAbleToSearchByLivesInP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.searchbycity(p1));
    }

    public void userAbleToSearchByWorksAtP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.searchbycompany(p1));
    }

    public void userAbleToSearchByWorksAsP1(String p1) {
    	Globalsearch  gs = new Globalsearch(Driverfactory.getSeleniumWebDriver());
    	assert(gs.searchbyposition(p1));
    }
    
    public void contactPresentInGlobalSearch() {

    }

    public void userAbleToSaveContactsUsingFROMMYNETWORK() {

    }

    public void userAbleToSaveContactsUsingONINTOUCHAPPSucessfully() {

    }

    public void userAbleToSearchContactPresentInContactListingPage() {

    }

    public void userAbleToSortContactsByFirstName() {

    }

    public void userAbleToSortContactsByLastName() {

    }

    public void userAbleToSortContactsByTimeAdded() {

    }

    public void userAbleToSortContactsByTimeModified() {

    }

    public void userAbleToSortContactsByOrganization() {

    }

    public void userAbleToScanBusinessCardSuccessfully() {

    }

    public void userAbleToAddNewListSuccessfully() {

    }

    public void userAbleToEditListSuccessfully() {

    }

    public void userAbleToAddContactsToTheList() {

    }

    public void userAbleToRemoveContactsPresentInList() {

    }

    public void userRemovesPreviouslyRegisteredAccountFromDevice() {      
    	/************************Manual Step*********************/   
    }


    
    public void userAbleToRegisterToInTouchAppByVerifyingOTP() {
    	/**********Enter OTP Manually and click on Continue**********************/
    	RegistraionPage rp = new RegistraionPage(Driverfactory.getSeleniumWebDriver());
        rp.registration();
    	
    }

    public void userAddsProfilePhotoWithP1ToTheProfile(String p1) {	
    	 RegistraionPage rp = new RegistraionPage(Driverfactory.getSeleniumWebDriver());
         rp.newregistration(); 	
         
    }

    public void userVerifiesContactSyncAfterRegistration() {
       ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
       cp.contacsync();	
    	
    	
    }

    public void userAbleToViewAllContactsPresentInPhonebookOnContactListingPageOfTheInTouchApp() {
    	 ContactsPage cp = new ContactsPage(Driverfactory.getSeleniumWebDriver());
         assert(cp.viewcontacts());
    	
    }

    public void userVerifyCardsAreDisplayedAfterAccountCreationAsPublicPersonal() {
    	HomePage hp = new HomePage(Driverfactory.getSeleniumWebDriver());
        hp.verifycardsvisibility();
    }

    public void userLaunchesInTouchAppByRegisteringUsingAnExistingUserP1(String p1) {
    	System.out.println("mobile Execution started");
		Driverfactory.SwitchDriver(1); 		
		Utilities.delayFor(1000);
        System.out.println("Apk is launched");
    	
    }

    public void verifyThatP1MatchesForTheExistingUserP2(String p1, String p2) {
    	RegistraionPage rp = new RegistraionPage(Driverfactory.getSeleniumWebDriver());
        rp.Verifyprofile(p1, p2);	
    }
    
    
}