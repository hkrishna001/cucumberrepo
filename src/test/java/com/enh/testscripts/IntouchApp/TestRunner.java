package com.enh.testscripts.IntouchApp;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@RunWith(Cucumber.class)
@CucumberOptions(features = {"./Features/Demo/Add_Edit_Delete_Contacts_pass.feature"}, 
					glue={""},
					format={"html:Results\\CucumberHTMLReports\\",
					"junit:Results\\CucumberXMLReports\\cucumberXML.xml",
					"json:Results\\CucumberJSONReports\\cucumber.json"}
				) 
public class TestRunner extends AbstractTestNGCucumberTests
{
	
}
