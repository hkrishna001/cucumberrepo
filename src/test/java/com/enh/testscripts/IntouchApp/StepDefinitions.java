package com.enh.testscripts.IntouchApp;

import org.testng.annotations.Test;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitions {
	
    public Actionwords actionwords = new Actionwords();
    @Test
    @Given("^User is registered In InTouchApp$")
    public void userIsRegisteredInInTouchApp() {
        actionwords.userIsRegisteredInInTouchApp();
    }

    @Then("^User navigates to Homepage$")
    public void userNavigatesToHomepage() {
        actionwords.userNavigatesToHomepage();
    }

    @Then("^User Adds Contact with following \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\"$")
    public void userAddsContactWithFollowingP1P2P3P4P5(String p1, String p2, String p3, String p4, String p5) {
        actionwords.userAddsContactWithFollowingP1P2P3P4P5(p1, p2, p3, p4, p5);
    }

    @Then("^User enters following company details \"(.*)\" \"(.*)\" \"(.*)\"$")
    public void userEntersFollowingCompanyDetailsP1P2P3(String p1, String p2, String p3) {
        actionwords.userEntersFollowingCompanyDetailsP1P2P3(p1, p2, p3);
    }

    @Then("^User enters how do you know them emoji context$")
    public void userEntersHowDoYouKnowThemEmojiContext() {
        actionwords.userEntersHowDoYouKnowThemEmojiContext();
    }

    @Then("^User enters required details \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\"$")
    public void userEntersRequiredDetailsP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13, String p14, String p15, String p16, String p17, String p18, String p19) {
        actionwords.userEntersRequiredDetailsP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19);
    }

    @Then("^User verifies the newly added Contact using \"(.*)\"$")
    public void userVerifiesTheNewlyAddedContactUsingP1(String p1) {
        actionwords.userVerifiesTheNewlyAddedContactUsingP1(p1);
    }

    @Then("^User navigates to contact listing page$")
    public void userNavigatesToContactListingPage() {
        actionwords.userNavigatesToContactListingPage();
    }

    @Then("^User dials a number from T9 Dialer using \"(.*)\"$")
    public void userDialsANumberFromT9DialerUsingP1(String p1) {
        actionwords.userDialsANumberFromT9DialerUsingP1(p1);
    }

    @Then("^User selects Greyring Image icon first number on Homepage Activity$")
    public void userSelectsGreyringImageIconFirstNumberOnHomepageActivity() {
        actionwords.userSelectsGreyringImageIconFirstNumberOnHomepageActivity();
    }

    @Then("^User adds new contact from T9 Dialer using \"(.*)\"$")
    public void userAddsNewContactFromT9DialerUsingP1(String p1) {
        actionwords.userAddsNewContactFromT9DialerUsingP1(p1);
    }

    @Then("^User dials a number using T9 dailer \"(.*)\"$")
    public void userDialsANumberUsingT9DailerP1(String p1) {
        actionwords.userDialsANumberUsingT9DailerP1(p1);
    }

    @Then("^User selects Greyring Image icon for existing user using \"(.*)\" on Homepage Activity$")
    public void userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(String p1) {
        actionwords.userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(p1);
    }

    @Then("^User verifies the newly added phonenumber for the \"(.*)\" \"(.*)\"$")
    public void userVerifiesTheNewlyAddedPhonenumberForTheP1P2(String p1,String p2) {
        actionwords.userVerifiesTheNewlyAddedPhonenumberForTheP1P2(p1,p2);
    }

  /*  @Then("^User adds to existing contact from T9 Dialer \"(.*)\"$")
    public void userAddsToExistingContactFromT9DialerP1() {
        actionwords.userAddsToExistingContactFromT9DialerP1();
    }*/
    
    @Then("^User verifies newly added phonenumber for the \"(.*)\" \"(.*)\"$")
    public void userVerifiesNewlyAddedPhonenumberForTheP1P2(String p1,String p2) {
        actionwords.userVerifiesNewlyAddedPhonenumberForTheP1P2(p1,p2);
    }

    @Then("^User Adds New Phone Contact with \"(.*)\"$")
    public void userAddsNewPhoneContactWithP1(String p1) {
        actionwords.userAddsNewPhoneContactWithP1(p1);
    }

    @Given("^User Launch InTouchApp$")
    public void userLaunchInTouchApp() {
        actionwords.userLaunchInTouchApp();
    }

    @When("^User searches contact by using \"(.*)\"$")
    public void userSearchesContactByUsingP1(String p1) {
        actionwords.userSearchesContactByUsingP1(p1);
    }

    @Then("^User Edits Contact with new details  \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\"  \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\"$")
    public void userEditsContactWithNewDetailsP1P2P3P4P5P6P7P8P9P10(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10) {
        actionwords.userEditsContactWithNewDetailsP1P2P3P4P5P6P7P8P9P10(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
    }

    @Then("^User Edits company details \"(.*)\" \"(.*)\"$")
    public void userEditsCompanyDetailsP1P2(String p1, String p2) {
        actionwords.userEditsCompanyDetailsP1P2(p1, p2);
    }

    @Then("^User Edits Phonenumber \"(.*)\" \"(.*)\"$")
    public void userEditsPhonenumberP1P2(String p1, String p2) {
        actionwords.userEditsPhonenumberP1P2(p1, p2);
    }

    @Then("^User verifies the Edit Contact using \"(.*)\"$")
    public void userVerifiesTheEditContactUsingP1(String p1) {
        actionwords.userVerifiesTheEditContactUsingP1(p1);
    }

    @Then("^User Share edited contact with ITA user and verify Push Message$")
    public void userShareEditedContactWithITAUserAndVerifyPushMessage() {
        actionwords.userShareEditedContactWithITAUserAndVerifyPushMessage();
    }

    @When("^User searches the edited contact using \"(.*)\"$")
    public void userSearchesTheEditedContactUsingP1(String p1) {
        actionwords.userSearchesTheEditedContactUsingP1(p1);
    }

    @When("^User Verifies Edited Contact on contact page$")
    public void userVerifiesEditedContactOnContactPage() {
        actionwords.userVerifiesEditedContactOnContactPage();
    }

    @When("^User Searches for contact using \"(.*)\"$")
    public void userSearchesForContactUsingP1(String p1) {
        actionwords.userSearchesForContactUsingP1(p1);
    }

    @Then("^contact should be displayed Successfully$")
    public void contactShouldBeDisplayedSuccessfully() {
        actionwords.contactShouldBeDisplayedSuccessfully();
    }

    @Then("^User should be able to delete from Contact View Page sucessfully$")
    public void userShouldBeAbleToDeleteFromContactViewPageSucessfully() {
        actionwords.userShouldBeAbleToDeleteFromContactViewPageSucessfully();
    }

    @Then("^User should be able to delete from contact listing page sucessfully$")
    public void userShouldBeAbleToDeleteFromContactListingPageSucessfully() {
        actionwords.userShouldBeAbleToDeleteFromContactListingPageSucessfully();
    }

    @Then("^User verifies that deleted Contacts are not present using \"(.*)\"$")
    public void userVerifiesThatDeletedContactsAreNotPresentUsingP1(String p1) {
        actionwords.userVerifiesThatDeletedContactsAreNotPresentUsingP1(p1);
    }

    @Then("^User able to see call activity$")
    public void userAbleToSeeCallActivity() {
        actionwords.userAbleToSeeCallActivity();
    }

    @Then("^User able to see connection request activity$")
    public void userAbleToSeeConnectionRequestActivity() {
        actionwords.userAbleToSeeConnectionRequestActivity();
    }

    @Then("^User able to see contact share activity$")
    public void userAbleToSeeContactShareActivity() {
        actionwords.userAbleToSeeContactShareActivity();
    }

    @Then("^User able to see notice-share activity$")
    public void userAbleToSeeNoticeshareActivity() {
        actionwords.userAbleToSeeNoticeshareActivity();
    }

    @Then("^User able to send message$")
    public void userAbleToSendMessage() {
        actionwords.userAbleToSendMessage();
    }

    @Then("^User able to set reminder$")
    public void userAbleToSetReminder() {
        actionwords.userAbleToSetReminder();
    }

    @Then("^User able to see activity log of the contact present on home page$")
    public void userAbleToSeeActivityLogOfTheContactPresentOnHomePage() {
        actionwords.userAbleToSeeActivityLogOfTheContactPresentOnHomePage();
    }

    @Then("^User able to open Game using On contact listing page$")
    public void userAbleToOpenGameUsingOnContactListingPage() {
        actionwords.userAbleToOpenGameUsingOnContactListingPage();
    }

    @Then("^User able to add emoji context to the contact$")
    public void userAbleToAddEmojiContextToTheContact() {
        actionwords.userAbleToAddEmojiContextToTheContact();
    }

    @Then("^User able to share score card on different app$")
    public void userAbleToShareScoreCardOnDifferentApp() {
        actionwords.userAbleToShareScoreCardOnDifferentApp();
    }

    @Then("^User able to set reminder for this functionality$")
    public void userAbleToSetReminderForThisFunctionality() {
        actionwords.userAbleToSetReminderForThisFunctionality();
    }

    @Then("^User able to see contact details of contact$")
    public void userAbleToSeeContactDetailsOfContact() {
        actionwords.userAbleToSeeContactDetailsOfContact();
    }

    @Then("^User able to rename the contact name$")
    public void userAbleToRenameTheContactName() {
        actionwords.userAbleToRenameTheContactName();
    }

    @Then("^User able to delete the contact$")
    public void userAbleToDeleteTheContact() {
        actionwords.userAbleToDeleteTheContact();
    }

    @Then("^User able to connect with InTouchApp users$")
    public void userAbleToConnectWithInTouchAppUsers() {
        actionwords.userAbleToConnectWithInTouchAppUsers();
    }

    @Then("^User Send request to ITA user and Verify$")
    public void userSendRequestToITAUserAndVerify() {
        actionwords.userSendRequestToITAUserAndVerify();
    }

    @Then("^User Navigates to mycards section$")
    public void userNavigatesToMycardsSection() {
        actionwords.userNavigatesToMycardsSection();
    }

    @Then("^User able to create new card with provided card data  \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\" \"(.*)\"$")
    public void userAbleToCreateNewCardWithProvidedCardDataP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19P20P21(String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9, String p10, String p11, String p12, String p13, String p14, String p15, String p16, String p17, String p18, String p19, String p20, String p21) {
        actionwords.userAbleToCreateNewCardWithProvidedCardDataP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19P20P21(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21);
    }

    @Then("^User able to edit card details of newly added card$")
    public void userAbleToEditCardDetailsOfNewlyAddedCard() {
        actionwords.userAbleToEditCardDetailsOfNewlyAddedCard();
    }

    @Then("^User able to share the card sucessfully$")
    public void userAbleToShareTheCardSucessfully() {
        actionwords.userAbleToShareTheCardSucessfully();
    }

    @Then("^User able to search contact present in phonebook$")
    public void userAbleToSearchContactPresentInPhonebook() {
        actionwords.userAbleToSearchContactPresentInPhonebook();
    }

    @Then("^User able to call the searched contact$")
    public void userAbleToCallTheSearchedContact() {
        actionwords.userAbleToCallTheSearchedContact();
    }

    @Then("^Send message to unknown number from T9 Search$")
    public void sendMessageToUnknownNumberFromT9Search() {
        actionwords.sendMessageToUnknownNumberFromT9Search();
    }

  /*  @Then("^User able to search Contacts present in phonebook \"(.*)\"$")
    public void userAbleToSearchContactsPresentInPhonebookp1(String p1) {
        actionwords.userAbleToSearchContactsPresentInPhonebookp1(p1);
    }

    @Then("^User able to search using InTouchApp user \"(.*)\"$")
    public void userAbleToSearchUsingInTouchAppUser(String p1) {
        actionwords.userAbleToSearchUsingInTouchAppUser(p1);
    }

    @Then("^User able to search using context \"(.*)\"$")
    public void userAbleToSearchUsingContext(String p1) {
        actionwords.userAbleToSearchUsingContext(p1);
    }

    @Then("^User able to search using emoji$")
    public void userAbleToSearchUsingEmoji() {
        actionwords.userAbleToSearchUsingEmoji();
    }

    @Then("^User able to search by lives in city name \"(.*)\"$")
    public void userAbleToSearchByLivesInCityName(String p1) {
        actionwords.userAbleToSearchByLivesInCityName(p1);
    }

    @Then("^User able to search by Works at company name \"(.*)\"$")
    public void userAbleToSearchByWorksAtCompanyName(String p1) {
        actionwords.userAbleToSearchByWorksAtCompanyName(p1);
    }

    @Then("^User able to search by Works as position \"(.*)\"$")
    public void userAbleToSearchByWorksAsPosition(String p1) {
        actionwords.userAbleToSearchByWorksAsPosition(p1);
    }*/
    
    @Then("^User able to search Contacts present in phonebook by using \"(.*)\"$")
    public void userAbleToSearchContactsPresentInPhonebookByUsingP1(String p1) {
        actionwords.userAbleToSearchContactsPresentInPhonebookByUsingP1(p1);
    }

    @Then("^User able to search using InTouchApp user by using \"(.*)\"$")
    public void userAbleToSearchUsingInTouchAppUserByUsingP1(String p1) {
        actionwords.userAbleToSearchUsingInTouchAppUserByUsingP1(p1);
    }

    @Then("^User able to search using \"(.*)\"$")
    public void userAbleToSearchUsingP1(String p1) {
        actionwords.userAbleToSearchUsingP1(p1);
    }

    @Then("^User able to search using emoji$")
    public void userAbleToSearchUsingEmoji() {
        actionwords.userAbleToSearchUsingEmoji();
    }

    @Then("^User able to search by lives in \"(.*)\"$")
    public void userAbleToSearchByLivesInP1(String p1) {
        actionwords.userAbleToSearchByLivesInP1(p1);
    }

    @Then("^User able to search by Works at \"(.*)\"$")
    public void userAbleToSearchByWorksAtP1(String p1) {
        actionwords.userAbleToSearchByWorksAtP1(p1);
    }

    @Then("^User able to search by Works as \"(.*)\"$")
    public void userAbleToSearchByWorksAsP1(String p1) {
        actionwords.userAbleToSearchByWorksAsP1(p1);
    }

    @When("^contact present in global search$")
    public void contactPresentInGlobalSearch() {
        actionwords.contactPresentInGlobalSearch();
    }

    @Then("^User able to save contacts using FROM MY NETWORK$")
    public void userAbleToSaveContactsUsingFROMMYNETWORK() {
        actionwords.userAbleToSaveContactsUsingFROMMYNETWORK();
    }

    @Then("^User able to save contacts using ON INTOUCHAPP sucessfully$")
    public void userAbleToSaveContactsUsingONINTOUCHAPPSucessfully() {
        actionwords.userAbleToSaveContactsUsingONINTOUCHAPPSucessfully();
    }

    @Then("^User able to search contact present in contact listing page$")
    public void userAbleToSearchContactPresentInContactListingPage() {
        actionwords.userAbleToSearchContactPresentInContactListingPage();
    }

    @Then("^User able to sort contacts by first name$")
    public void userAbleToSortContactsByFirstName() {
        actionwords.userAbleToSortContactsByFirstName();
    }

    @Then("^User able to sort contacts by last name$")
    public void userAbleToSortContactsByLastName() {
        actionwords.userAbleToSortContactsByLastName();
    }

    @Then("^User able to sort contacts by time added$")
    public void userAbleToSortContactsByTimeAdded() {
        actionwords.userAbleToSortContactsByTimeAdded();
    }

    @Then("^User able to sort contacts by time modified$")
    public void userAbleToSortContactsByTimeModified() {
        actionwords.userAbleToSortContactsByTimeModified();
    }

    @Then("^User able to sort contacts by organization$")
    public void userAbleToSortContactsByOrganization() {
        actionwords.userAbleToSortContactsByOrganization();
    }

    @Then("^User able to scan business card Successfully$")
    public void userAbleToScanBusinessCardSuccessfully() {
        actionwords.userAbleToScanBusinessCardSuccessfully();
    }

    @Then("^User able to add new list Successfully$")
    public void userAbleToAddNewListSuccessfully() {
        actionwords.userAbleToAddNewListSuccessfully();
    }

    @Then("^User able to edit list Successfully$")
    public void userAbleToEditListSuccessfully() {
        actionwords.userAbleToEditListSuccessfully();
    }

    @Then("^User able to add contacts to the list$")
    public void userAbleToAddContactsToTheList() {
        actionwords.userAbleToAddContactsToTheList();
    }

    @Then("^User able to remove contacts present in list$")
    public void userAbleToRemoveContactsPresentInList() {
        actionwords.userAbleToRemoveContactsPresentInList();
    }

    @Given("^User removes previously registered account from device$")
    public void userRemovesPreviouslyRegisteredAccountFromDevice() {
        actionwords.userRemovesPreviouslyRegisteredAccountFromDevice();
    }

    @Then("^User able to Register to InTouchApp by verifying OTP$")
    public void userAbleToRegisterToInTouchAppByVerifyingOTP() {
        actionwords.userAbleToRegisterToInTouchAppByVerifyingOTP();
    }

    @Then("^User adds profile photo with \"(.*)\" to the profile$")
    public void userAddsProfilePhotoWithP1ToTheProfile(String p1) {
        actionwords.userAddsProfilePhotoWithP1ToTheProfile(p1);
    }

    @Then("^User verifies contact sync after Registration$")
    public void userVerifiesContactSyncAfterRegistration() {
        actionwords.userVerifiesContactSyncAfterRegistration();
    }

    @Then("^User able to view all contacts present in phonebook on contact listing page of the InTouchApp$")
    public void userAbleToViewAllContactsPresentInPhonebookOnContactListingPageOfTheInTouchApp() {
        actionwords.userAbleToViewAllContactsPresentInPhonebookOnContactListingPageOfTheInTouchApp();
    }

    @Then("^User verify cards are displayed after account creation as public personal$")
    public void userVerifyCardsAreDisplayedAfterAccountCreationAsPublicPersonal() {
        actionwords.userVerifyCardsAreDisplayedAfterAccountCreationAsPublicPersonal();
    }

    @Then("^User Launches InTouchApp by registering using an existing user \"(.*)\"$")
    public void userLaunchesInTouchAppByRegisteringUsingAnExistingUserP1(String p1) {
        actionwords.userLaunchesInTouchAppByRegisteringUsingAnExistingUserP1(p1);
    }

    @Then("^verify that \"(.*)\" matches for the existing user \"(.*)\"$")
    public void verifyThatP1MatchesForTheExistingUserP2(String p1, String p2) {
        actionwords.verifyThatP1MatchesForTheExistingUserP2(p1, p2);
    }
}