// every java.util.Date class in the vet package should be
// processed by DateAdapter
@XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class)
package com.enh.framework.testreports;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.enh.framework.core.DateAdapter;

import java.util.Date;

