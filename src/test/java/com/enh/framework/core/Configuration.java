package com.enh.framework.core;

import java.io.FileInputStream;
import java.util.Properties;

import com.enh.framework.testreports.ReportManager;

public class Configuration {
	
	private static Properties prop; 
	
	public static String getProperty(String key)
	{
		try{
			if (prop == null)
			{
				prop = new Properties();
				FileInputStream fis = new FileInputStream("./Resource/properties.xml");
				prop.loadFromXML(fis);
			}
			return prop.getProperty(key);
		}
		catch (Exception ex){
			ReportManager.LogFailure("Load Configuration", "Loading test automation configuration file", "test automation configuration file should be loaded", ex.getMessage() + ": Exception occured while loading script configuration", false);
			return null;
		}
	}
}
