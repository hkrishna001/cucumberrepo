package com.enh.framework.core;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.enh.framework.testreports.ReportManager;


public class Utilities {

	final static String alphaNumerics = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";
	final static String alphabets = "abcdefghijklmnopqrstuvwxyz";

	final static Random rand = new Random();

	// consider using a Map<String,Boolean> to say whether the identifier is being used or not 
	final static Set<String> identifiers = new HashSet<String>();
	

	/*******************************************************
	 * Function Name: captureScreen
	 * Description : Screenshots are captured in this function
	 ********************************************************/
	
	public static void captureScreen(String fileName) {
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Rectangle screenRectangle = new Rectangle(screenSize);
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(screenRectangle);
			File tmpFile = new File(fileName);
			System.out.println(tmpFile.getAbsolutePath());
			ImageIO.write(image, "png", tmpFile);
		} catch (Exception ex) {
			ReportManager.LogFailure("Framework Function", "Capture Screenshot", "",
					"Exception occured while capturing screen shot. Exception message is: " + ex.getMessage(), false);
		}
	}
	/*******************************************************
	 * Function Name: getDuration
	 * Description : Execution time/duration is captured in this function
	 ********************************************************/
	public static String getDuration(Date start_Date, Date end_Date) {
		long diff = end_Date.getTime() - start_Date.getTime();
		long diffhrs = diff / (60 * 60 * 1000);
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffSecs = diff / (1000) % 60 % 60;
		return diffhrs + " Hrs " + diffMinutes + " Mins " + diffSecs + " secs.";

	}
	
	/******************************************************************************************
	 * Function Name: focusOnElement
	 * Description :  This function is used for focusing/placing the pointer on the webelement 
	 *******************************************************************************************/

	
	
	/**********************************************************************************************
	 * Function Name	: generateRandomStringwithNumerics
	 * Description 	: This function is used for generating random string value 
	 * 					consisting of Characters and numerics 
	 ***********************************************************************************************/
	
	
	public static String generateRandomStringwithNumerics()
	{
		StringBuilder builder = new StringBuilder();
		try
		{
			while(builder.toString().length() == 0) 
			{
				for(int i = 0; i < 5; i++) 
				{
					builder.append(alphaNumerics.charAt(rand.nextInt(alphaNumerics.length())));
				}
				if(identifiers.contains(builder.toString())) {
					builder = new StringBuilder();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return builder.toString();
	}

	/**********************************************************************************************
	 * Function Name	: generateRandomStringwithNumerics
	 * Description 	: This function is used for generating random string value 
	 * 					consisting of only Characters 
	 ***********************************************************************************************/

	public static String generateRandomStringwithoutNumerics()
	{
		StringBuilder builder = new StringBuilder();
		try
		{
			while(builder.toString().length() == 0) 
			{
				for(int i = 0; i < 5; i++) 
				{
					builder.append(alphabets.charAt(rand.nextInt(alphabets.length())));
				}
				if(identifiers.contains(builder.toString())) {
					builder = new StringBuilder();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return builder.toString();
	}
	/******************************************************************************************
	 * Function Name: delayFor
	 * Description :  This function is used for delaying or waiting for given amount of time
	 ******************************************************************************************/

	public static void delayFor(long milliSec) {

		try {
			Thread.sleep(milliSec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/***************************************************************
	 * Function Name: InvokeProcess
	 * Description :  This function is used for invoking a process
	 ***************************************************************/
	public static void InvokeProcess(String ProcessName, String args) {

		try {
			ProcessBuilder p = new ProcessBuilder();
			p.command(ProcessName, args);
			p.start();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	/*****************************************************************************
	 * Function Name: trasformXMLtoHTML
	 * Description :  This function is used for converting the xml code to html 
	 *****************************************************************************/
	public static void trasformXMLtoHTML(String xmlFilePath, String xsltFilePath, String outHTMLFilePath) {
		Document document;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			File stylesheet = new File(xsltFilePath);
			File datafile = new File(xmlFilePath);
			document = builder.parse(datafile);

			// Use a Transformer for output
			TransformerFactory tFactory = TransformerFactory.newInstance();
			StreamSource stylesource = new StreamSource(stylesheet);
			Transformer transformer = tFactory.newTransformer(stylesource);

			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new File(outHTMLFilePath));
			transformer.transform(source, result);
		} catch (TransformerConfigurationException tce) {
			// Error generated by the parser
			System.out.println("\n** Transformer Factory error");
			System.out.println("   " + tce.getMessage());

			// Use the contained exception, if any
			Throwable x = tce;

			if (tce.getException() != null) {
				x = tce.getException();
			}

			x.printStackTrace();
		} catch (TransformerException te) {
			// Error generated by the parser
			System.out.println("\n** Transformation error");
			System.out.println("   " + te.getMessage());

			// Use the contained exception, if any
			Throwable x = te;

			if (te.getException() != null) {
				x = te.getException();
			}

			x.printStackTrace();
		} catch (SAXException sxe) {
			// Error generated by this application
			// (or a parser-initialization error)
			Exception x = sxe;

			if (sxe.getException() != null) {
				x = sxe.getException();
			}

			x.printStackTrace();
		} catch (ParserConfigurationException pce) {
			// Parser with specified options can't be built
			pce.printStackTrace();
		} catch (IOException ioe) {
			// I/O error
			ioe.printStackTrace();
		}
	} // main

}
