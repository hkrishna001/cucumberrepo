package com.enh.framework.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.sun.jna.Platform;

import io.appium.java_client.android.AndroidDriver;

public class Driverfactory {

	
	
	
	/**********  Switch Driver To Web Mobile or APK  ************/

	public static WebDriver driver;

	public static WebDriver getSeleniumWebDriver()
	{
		if (driver == null)
		{
		
		}
		return driver;
	}
	
	
	/*********Switch to 3.webdesktop  	2.Mobile Browser  1.Mobile apk*********/
	public static WebDriver SwitchDriver(int drivercode)
	{
		/****Properties.xml******/
		String webBrowser = Configuration.getProperty("WebBrowser");
		String mobBrowser = Configuration.getProperty("MobBrowser");
		
		
		driver = getSeleniumWebDriver();
		System.out.println("Driver value");
		System.out.println(driver);
		if (drivercode == 1 && driver == null) {
			
			File app = new File("Misc//intouchapp-gp-test-3.2.8.apk");     /*Apk Path*/ 
			DesiredCapabilities caps = DesiredCapabilities.android();
//			caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.ANDROID);
			caps.setCapability("platformName", "Android");
//			caps.setCapability(CapabilityType.PLATFORM, Platform.ANDROID);
			caps.setCapability("deviceName", "ELITE");
			caps.setCapability(CapabilityType.VERSION, "4.4.4");
			caps.setCapability("app", app.getAbsolutePath());
			caps.setCapability("unicodeKeyboard", true);
			caps.setCapability("resetKeyboard", true);

			URL remoteAddress;
			try {	
				remoteAddress = new URL("http://127.0.0.1:4723/wd/hub");
				driver = new AndroidDriver<WebElement> (remoteAddress, caps) ;
				driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if ((drivercode == 2)) {
			if(mobBrowser.equalsIgnoreCase("Android")) {
				DesiredCapabilities caps = DesiredCapabilities.android();
				caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.ANDROID);
				caps.setCapability("platformName", "Android");
				caps.setCapability(CapabilityType.PLATFORM, Platform.ANDROID);
				caps.setCapability("deviceName", "Google Nexus5");
				caps.setCapability(CapabilityType.VERSION, "6.0.1");
				caps.setCapability("app", "Browser");
				caps.setCapability("newCommandTimeout", 10000);
				caps.setCapability("unicodeKeyboard", true);
				caps.setCapability("resetKeyboard", true);
				
				URL remoteAddress;
				try {	
					remoteAddress = new URL("http://127.0.0.1:4723/wd/hub");
					driver = new RemoteWebDriver (remoteAddress, caps) ;
					driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}


			else if (mobBrowser.equalsIgnoreCase("Chrome")) {
				DesiredCapabilities caps = DesiredCapabilities.android();
				caps.setCapability(CapabilityType.BROWSER_NAME, BrowserType.ANDROID);
				caps.setCapability("platformName", "Android");
				caps.setCapability(CapabilityType.PLATFORM, Platform.ANDROID);
				caps.setCapability("deviceName", "Google Nexus5");
				caps.setCapability(CapabilityType.VERSION, "6.0.1");
				caps.setCapability("app", "Chrome");
				caps.setCapability("newCommandTimeout", 10000);

				URL remoteAddress;
				try {	
					remoteAddress = new URL("http://127.0.0.1:4723/wd/hub");
					driver = new RemoteWebDriver (remoteAddress, caps) ;
					driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		/**********Add PC browser Below Eg: Chrome, FF,IE,Safari as req************/
		if ((drivercode == 3)) {
			if(webBrowser.equalsIgnoreCase("Chrome")) {
			File file = new File("./Resource/Drivers/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			ChromeOptions chromeOptions = new ChromeOptions();
			//chromeOptions.addExcludedArgument("ignore-certifcate-errors");

			//				added new line
			chromeOptions.addExtensions(new File("E:\\InTouchApp-for-WhatsAppWeb+LinkedIn-Save+Call_v2.0.9.crx"));
			chromeOptions.addArguments("test-type");
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
			caps.setCapability("ignoreProtectedModeSettings", true);
			driver = new ChromeDriver(caps);

		}
		}

		Utilities.delayFor(1000);
		/**Android Wont Accept focus**/
/**		((JavascriptExecutor) driver).executeScript("window.focus();");  **/
 		return driver;
	}

}