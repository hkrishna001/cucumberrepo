package com.enh.framework.core;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.util.concurrent.TimeUnit;

import com.enh.framework.testreports.ReportManager;

import cucumber.api.Scenario;


public class BaseTest{
	
	public static String scenarioName;

	@BeforeMethod
	public void initializeTestCase(Scenario scenario) {
		if(scenario.getName().toString().equalsIgnoreCase(scenarioName))
		{
			
		}
		else
		{
			ReportManager.initalizeTestCase(scenario.getName().toString(),scenario.getName().toString());
		}
		
	}
	
	@BeforeMethod
	public void SuiteInit(Scenario scenario) 
	{
		ReportManager.intializeTestReport("Cucumber Test Report");
		scenarioName = scenario.getName().toString();
		ReportManager.initalizeTestCase(scenario.getName().toString(),scenario.getName().toString());		
		
		
		/*Windows browser code Ex: chrome,FF     uncomment */		
		/****ExecutionEngine.CreateSeleniumWebDriver();
		ExecutionEngine.getSeleniumWebDriver().navigate().to(Configuration.getProperty("URL"));
		ExecutionEngine.getSeleniumWebDriver().switchTo().window("");
		ExecutionEngine.getSeleniumWebDriver().manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);****/
		
		
		Utilities.delayFor(3000);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				
    				/****ExecutionEngine.getSeleniumWebDriver().close();
			        /ExecutionEngine.getSeleniumWebDriver().quit();****/
				    ReportManager.SaveTestReport();
			}     
		});
	}
	
	@AfterMethod
	public void SuiteCleanup() 
	{
		System.out.println("In suite Cleanup");
		try {
			ReportManager.ClostTestCaseReport();
//			ExecutionEngine.getSeleniumWebDriver().close();
			//ExecutionEngine.getSeleniumWebDriver().quit();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}