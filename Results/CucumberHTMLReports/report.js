$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("./Features/Demo/Add_Edit_Delete_Contacts_pass.feature");
formatter.feature({
  "line": 1,
  "name": "Add Edit Delete Contacts",
  "description": "",
  "id": "add-edit-delete-contacts",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Add New Contact from HomePage Activity List",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-from-homepage-activity-list",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User dials a number from T9 Dialer using \"\u003cphoneno\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User selects Greyring Image icon first number on Homepage Activity",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User Adds Contact with following \"\u003cprefix\u003e\" \"\u003cfirstname\u003e\" \"\u003cmiddlename\u003e\" \"\u003clastname\u003e\" \"\u003csuffix\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "User enters following company details \"\u003ccompany\u003e\" \"\u003cposition\u003e\" \"\u003cdepartment\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User enters how do you know them emoji context",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User enters required details \"\u003cphonecategory\u003e\" \"\u003cphonetype\u003e\" \"\u003cphoneno\u003e\" \"\u003cemailtype\u003e\" \"\u003cemailid\u003e\" \"\u003curltype\u003e\" \"\u003curl\u003e\" \"\u003cdatetype\u003e\" \"\u003cdate\u003e\" \"\u003csocialurltype\u003e\" \"\u003csocialurl\u003e\" \"\u003caddresstype\u003e\" \"\u003chouseno\u003e\" \"\u003clocality\u003e\" \"\u003ccity\u003e\" \"\u003cpincode\u003e\" \"\u003ccountry\u003e\" \"\u003cstate\u003e\" \"\u003cnotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User verifies the newly added Contact using \"\u003cfirstname\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-from-homepage-activity-list;",
  "rows": [
    {
      "cells": [
        "prefix",
        "firstname",
        "middlename",
        "lastname",
        "suffix",
        "company",
        "position",
        "department",
        "phonecategory",
        "phonetype",
        "phoneno",
        "emailtype",
        "emailid",
        "urltype",
        "url",
        "datetype",
        "date",
        "socialurltype",
        "socialurl",
        "addresstype",
        "houseno",
        "locality",
        "city",
        "pincode",
        "country",
        "state",
        "notes",
        "hiptestuid",
        "hiptest-uid"
      ],
      "line": 14,
      "id": "add-edit-delete-contacts;add-new-contact-from-homepage-activity-list;;1"
    },
    {
      "cells": [
        "Mr.",
        "Omc",
        "Usr",
        "Three",
        "lee",
        "Intouch",
        "Tester3",
        "QA",
        "Personal",
        "Mobile",
        "0734951279",
        "Personal",
        "testusertw@Intouch.com",
        "Personal",
        "www.intouchapp.com",
        "Biryhday",
        "Aug/01/1992",
        "Facebook",
        "www.fbintouchapp",
        "Personal",
        "217445",
        "LancoHills",
        "Hyderabad",
        "500014",
        "India",
        "Telangana",
        "Skipphone",
        "",
        ""
      ],
      "line": 15,
      "id": "add-edit-delete-contacts;add-new-contact-from-homepage-activity-list;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 15,
  "name": "Add New Contact from HomePage Activity List",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-from-homepage-activity-list;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User dials a number from T9 Dialer using \"0734951279\"",
  "matchedColumns": [
    10
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 6,
  "name": "User selects Greyring Image icon first number on Homepage Activity",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "User Adds Contact with following \"Mr.\" \"Omc\" \"Usr\" \"Three\" \"lee\"",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "User enters following company details \"Intouch\" \"Tester3\" \"QA\"",
  "matchedColumns": [
    5,
    6,
    7
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "User enters how do you know them emoji context",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "User enters required details \"Personal\" \"Mobile\" \"0734951279\" \"Personal\" \"testusertw@Intouch.com\" \"Personal\" \"www.intouchapp.com\" \"Biryhday\" \"Aug/01/1992\" \"Facebook\" \"www.fbintouchapp\" \"Personal\" \"217445\" \"LancoHills\" \"Hyderabad\" \"500014\" \"India\" \"Telangana\" \"Skipphone\"",
  "matchedColumns": [
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "User verifies the newly added Contact using \"Omc\"",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.userIsRegisteredInInTouchApp()"
});
formatter.result({
  "duration": 66304242575,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0734951279",
      "offset": 42
    }
  ],
  "location": "StepDefinitions.userDialsANumberFromT9DialerUsingP1(String)"
});
formatter.result({
  "duration": 21981377242,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitions.userSelectsGreyringImageIconFirstNumberOnHomepageActivity()"
});
formatter.result({
  "duration": 2619124579,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mr.",
      "offset": 34
    },
    {
      "val": "Omc",
      "offset": 40
    },
    {
      "val": "Usr",
      "offset": 46
    },
    {
      "val": "Three",
      "offset": 52
    },
    {
      "val": "lee",
      "offset": 60
    }
  ],
  "location": "StepDefinitions.userAddsContactWithFollowingP1P2P3P4P5(String,String,String,String,String)"
});
formatter.result({
  "duration": 33766221784,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Intouch",
      "offset": 39
    },
    {
      "val": "Tester3",
      "offset": 49
    },
    {
      "val": "QA",
      "offset": 59
    }
  ],
  "location": "StepDefinitions.userEntersFollowingCompanyDetailsP1P2P3(String,String,String)"
});
formatter.result({
  "duration": 22226881740,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinitions.userEntersHowDoYouKnowThemEmojiContext()"
});
formatter.result({
  "duration": 6691041253,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Personal",
      "offset": 30
    },
    {
      "val": "Mobile",
      "offset": 41
    },
    {
      "val": "0734951279",
      "offset": 50
    },
    {
      "val": "Personal",
      "offset": 63
    },
    {
      "val": "testusertw@Intouch.com",
      "offset": 74
    },
    {
      "val": "Personal",
      "offset": 99
    },
    {
      "val": "www.intouchapp.com",
      "offset": 110
    },
    {
      "val": "Biryhday",
      "offset": 131
    },
    {
      "val": "Aug/01/1992",
      "offset": 142
    },
    {
      "val": "Facebook",
      "offset": 156
    },
    {
      "val": "www.fbintouchapp",
      "offset": 167
    },
    {
      "val": "Personal",
      "offset": 186
    },
    {
      "val": "217445",
      "offset": 197
    },
    {
      "val": "LancoHills",
      "offset": 206
    },
    {
      "val": "Hyderabad",
      "offset": 219
    },
    {
      "val": "500014",
      "offset": 231
    },
    {
      "val": "India",
      "offset": 240
    },
    {
      "val": "Telangana",
      "offset": 248
    },
    {
      "val": "Skipphone",
      "offset": 260
    }
  ],
  "location": "StepDefinitions.userEntersRequiredDetailsP1P2P3P4P5P6P7P8P9P10P11P12P13P14P15P16P17P18P19(String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 157050337969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Omc",
      "offset": 45
    }
  ],
  "location": "StepDefinitions.userVerifiesTheNewlyAddedContactUsingP1(String)"
});
formatter.result({
  "duration": 17410047799,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 18,
  "name": "Add New Contact to existing contact in HomePage",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-homepage",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 19,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "User dials a number using T9 dailer \"\u003cnewphoneno\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "User selects Greyring Image icon for existing user using \"\u003cfirstname\u003e\" on Homepage Activity",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "User verifies the newly added phonenumber for the \"\u003cfirstname\u003e\" \"\u003cnewphoneno\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 24,
  "name": "",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-homepage;",
  "rows": [
    {
      "cells": [
        "firstname",
        "newphoneno",
        "hiptestuid",
        "hiptest-uid"
      ],
      "line": 25,
      "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-homepage;;1"
    },
    {
      "cells": [
        "Omc",
        "1800500235",
        "",
        ""
      ],
      "line": 26,
      "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-homepage;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 26,
  "name": "Add New Contact to existing contact in HomePage",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-homepage;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 19,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "User dials a number using T9 dailer \"1800500235\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "User selects Greyring Image icon for existing user using \"Omc\" on Homepage Activity",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "User verifies the newly added phonenumber for the \"Omc\" \"1800500235\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.userIsRegisteredInInTouchApp()"
});
formatter.result({
  "duration": 84574823135,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1800500235",
      "offset": 37
    }
  ],
  "location": "StepDefinitions.userDialsANumberUsingT9DailerP1(String)"
});
formatter.result({
  "duration": 43018893646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Omc",
      "offset": 58
    }
  ],
  "location": "StepDefinitions.userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(String)"
});
formatter.result({
  "duration": 47784438392,
  "error_message": "org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 40.22 seconds\nBuild info: version: \u0027unknown\u0027, revision: \u002731c43c8\u0027, time: \u00272016-08-02 21:57:56 -0700\u0027\nSystem info: host: \u0027ESW007\u0027, ip: \u0027192.168.11.2\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: io.appium.java_client.android.AndroidDriver\nCapabilities [{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, networkConnectionEnabled\u003dtrue, warnings\u003d{}, databaseEnabled\u003dfalse, version\u003d4.4.4, deviceName\u003d060331420a28ed5f, platform\u003dANDROID, desired\u003d{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, browserName\u003dandroid, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, version\u003d4.4.4, deviceName\u003dELITE, platform\u003dANDROID, resetKeyboard\u003dtrue}, platformVersion\u003d6.0.1, webStorageEnabled\u003dfalse, locationContextEnabled\u003dfalse, browserName\u003dandroid, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, resetKeyboard\u003dtrue}]\nSession ID: 6f09b2ec-c386-45eb-be8f-92a4dcea206d\n*** Element info: {Using\u003did, value\u003dgrey_ring}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:683)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:40)\r\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:377)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:56)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:427)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElementById(DefaultGenericMobileDriver.java:64)\r\n\tat io.appium.java_client.AppiumDriver.findElementById(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElementById(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:369)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:52)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat com.enh.mobile.page.HomePage.GreyRingImageselect(HomePage.java:226)\r\n\tat com.enh.testscripts.IntouchApp.Actionwords.userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(Actionwords.java:138)\r\n\tat com.enh.testscripts.IntouchApp.StepDefinitions.userSelectsGreyringImageIconForExistingUserUsingP1OnHomepageActivity(StepDefinitions.java:75)\r\n\tat ✽.Then User selects Greyring Image icon for existing user using \"Omc\" on Homepage Activity(./Features/Demo/Add_Edit_Delete_Contacts_pass.feature:21)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Omc",
      "offset": 51
    },
    {
      "val": "1800500235",
      "offset": 57
    }
  ],
  "location": "StepDefinitions.userVerifiesTheNewlyAddedPhonenumberForTheP1P2(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenarioOutline({
  "line": 28,
  "name": "Verify the Added or Edited Contact",
  "description": "",
  "id": "add-edit-delete-contacts;verify-the-added-or-edited-contact",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 29,
  "name": "User Launch InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 30,
  "name": "User searches the edited contact using \"\u003cfirstname\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "User Verifies Edited Contact on contact page",
  "keyword": "And "
});
formatter.examples({
  "line": 33,
  "name": "",
  "description": "",
  "id": "add-edit-delete-contacts;verify-the-added-or-edited-contact;",
  "rows": [
    {
      "cells": [
        "firstname",
        "hiptestuid",
        "hiptest-uid"
      ],
      "line": 34,
      "id": "add-edit-delete-contacts;verify-the-added-or-edited-contact;;1"
    },
    {
      "cells": [
        "Omc",
        "",
        ""
      ],
      "line": 35,
      "id": "add-edit-delete-contacts;verify-the-added-or-edited-contact;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 35,
  "name": "Verify the Added or Edited Contact",
  "description": "",
  "id": "add-edit-delete-contacts;verify-the-added-or-edited-contact;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 29,
  "name": "User Launch InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 30,
  "name": "User searches the edited contact using \"Omc\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "User Verifies Edited Contact on contact page",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.userLaunchInTouchApp()"
});
formatter.result({
  "duration": 11000483127,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Omc",
      "offset": 40
    }
  ],
  "location": "StepDefinitions.userSearchesTheEditedContactUsingP1(String)"
});
formatter.result({
  "duration": 40349755137,
  "error_message": "org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 40.35 seconds\nBuild info: version: \u0027unknown\u0027, revision: \u002731c43c8\u0027, time: \u00272016-08-02 21:57:56 -0700\u0027\nSystem info: host: \u0027ESW007\u0027, ip: \u0027192.168.11.2\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: io.appium.java_client.android.AndroidDriver\nCapabilities [{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, networkConnectionEnabled\u003dtrue, warnings\u003d{}, databaseEnabled\u003dfalse, version\u003d4.4.4, deviceName\u003d060331420a28ed5f, platform\u003dANDROID, desired\u003d{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, browserName\u003dandroid, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, version\u003d4.4.4, deviceName\u003dELITE, platform\u003dANDROID, resetKeyboard\u003dtrue}, platformVersion\u003d6.0.1, webStorageEnabled\u003dfalse, locationContextEnabled\u003dfalse, browserName\u003dandroid, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, resetKeyboard\u003dtrue}]\nSession ID: 6f09b2ec-c386-45eb-be8f-92a4dcea206d\n*** Element info: {Using\u003did, value\u003dmenu_contacts}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:683)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:40)\r\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:377)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:56)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:427)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElementById(DefaultGenericMobileDriver.java:64)\r\n\tat io.appium.java_client.AppiumDriver.findElementById(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElementById(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:369)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:52)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat com.enh.mobile.page.ContactsPage.EditedContactSearch(ContactsPage.java:136)\r\n\tat com.enh.testscripts.IntouchApp.Actionwords.userSearchesTheEditedContactUsingP1(Actionwords.java:229)\r\n\tat com.enh.testscripts.IntouchApp.StepDefinitions.userSearchesTheEditedContactUsingP1(StepDefinitions.java:135)\r\n\tat ✽.When User searches the edited contact using \"Omc\"(./Features/Demo/Add_Edit_Delete_Contacts_pass.feature:30)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "StepDefinitions.userVerifiesEditedContactOnContactPage()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenarioOutline({
  "line": 37,
  "name": "Add New Contact to existing contact in T9 Dialer",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-t9-dialer",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 38,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "User dials a number using T9 dailer \"\u003cnewphoneno\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "User verifies newly added phonenumber for the \"\u003cfirstname\u003e\" \"\u003cnewphoneno\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "line": 42,
  "name": "",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-t9-dialer;",
  "rows": [
    {
      "cells": [
        "firstname",
        "newphoneno",
        "hiptestuid",
        "hiptest-uid"
      ],
      "line": 43,
      "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-t9-dialer;;1"
    },
    {
      "cells": [
        "Omc",
        "0022555846",
        "",
        ""
      ],
      "line": 44,
      "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-t9-dialer;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 44,
  "name": "Add New Contact to existing contact in T9 Dialer",
  "description": "",
  "id": "add-edit-delete-contacts;add-new-contact-to-existing-contact-in-t9-dialer;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 38,
  "name": "User is registered In InTouchApp",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "User dials a number using T9 dailer \"0022555846\"",
  "matchedColumns": [
    1
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "User verifies newly added phonenumber for the \"Omc\" \"0022555846\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.userIsRegisteredInInTouchApp()"
});
formatter.result({
  "duration": 84510295962,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "0022555846",
      "offset": 37
    }
  ],
  "location": "StepDefinitions.userDialsANumberUsingT9DailerP1(String)"
});
formatter.result({
  "duration": 40428154073,
  "error_message": "org.openqa.selenium.WebDriverException: An unknown server-side error occurred while processing the command. (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 40.43 seconds\nBuild info: version: \u0027unknown\u0027, revision: \u002731c43c8\u0027, time: \u00272016-08-02 21:57:56 -0700\u0027\nSystem info: host: \u0027ESW007\u0027, ip: \u0027192.168.11.2\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_111\u0027\nDriver info: io.appium.java_client.android.AndroidDriver\nCapabilities [{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, networkConnectionEnabled\u003dtrue, warnings\u003d{}, databaseEnabled\u003dfalse, version\u003d4.4.4, deviceName\u003d060331420a28ed5f, platform\u003dANDROID, desired\u003d{app\u003dD:\\Intouch\\CucumberDemo\\Misc\\intouchapp-gp-test-3.2.8.apk, browserName\u003dandroid, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, version\u003d4.4.4, deviceName\u003dELITE, platform\u003dANDROID, resetKeyboard\u003dtrue}, platformVersion\u003d6.0.1, webStorageEnabled\u003dfalse, locationContextEnabled\u003dfalse, browserName\u003dandroid, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, unicodeKeyboard\u003dtrue, platformName\u003dAndroid, resetKeyboard\u003dtrue}]\nSession ID: 6f09b2ec-c386-45eb-be8f-92a4dcea206d\n*** Element info: {Using\u003did, value\u003dmenu_dialer}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:683)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:40)\r\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:377)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:56)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementById(RemoteWebDriver.java:427)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElementById(DefaultGenericMobileDriver.java:64)\r\n\tat io.appium.java_client.AppiumDriver.findElementById(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElementById(AndroidDriver.java:1)\r\n\tat org.openqa.selenium.By$ById.findElement(By.java:218)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:369)\r\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:52)\r\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\r\n\tat io.appium.java_client.android.AndroidDriver.findElement(AndroidDriver.java:1)\r\n\tat com.enh.mobile.page.DailerPage.DialCustomNumberT9Dialer(DailerPage.java:128)\r\n\tat com.enh.testscripts.IntouchApp.Actionwords.userDialsANumberUsingT9DailerP1(Actionwords.java:130)\r\n\tat com.enh.testscripts.IntouchApp.StepDefinitions.userDialsANumberUsingT9DailerP1(StepDefinitions.java:70)\r\n\tat ✽.Then User dials a number using T9 dailer \"0022555846\"(./Features/Demo/Add_Edit_Delete_Contacts_pass.feature:39)\r\n",
  "status": "failed"
});
formatter.match({
  "arguments": [
    {
      "val": "Omc",
      "offset": 47
    },
    {
      "val": "0022555846",
      "offset": 53
    }
  ],
  "location": "StepDefinitions.userVerifiesNewlyAddedPhonenumberForTheP1P2(String,String)"
});
formatter.result({
  "status": "skipped"
});
});